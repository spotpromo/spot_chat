package com.spotpromo.spotchat.recycler

import android.content.Context
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.spotpromo.spotchat.R
import com.spotpromo.spotchat.model.Contato
import com.spotpromo.spotchat.utils.LogTrace
import java.lang.String
import java.util.*

class RecycleListaMensagem(val mContext: Context, val mLista: ArrayList<Contato?>) : RecyclerView.Adapter<RecycleListaMensagem.ViewHolder>() {


    class ViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        var lnStatus: LinearLayout = v.findViewById<View>(R.id.ln_status) as LinearLayout
        var mRow: LinearLayout = v.findViewById<View>(R.id.mrow) as LinearLayout
        var txtDados: TextView = v.findViewById<View>(R.id.txt_dados) as TextView

    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ViewHolder {
        val view: View = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.recycle_generic, viewGroup, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, i: Int) {
        try {
            val contato: Contato = mLista!![i]!!
            val sbHtml = StringBuilder()
            sbHtml.append(String.format("%s", contato.getNome()))
            holder.txtDados.text = Html.fromHtml(sbHtml.toString())
            /**
             * VERIFICA STATUS COLETA
             */
            if (contato.getLido() != null) {
                when (contato.getLido()) {
                    1 -> {
                        holder.lnStatus.setBackgroundColor(mContext!!.resources.getColor(R.color.verde))
                        holder.lnStatus.invalidate()
                    }
                    else -> {
                        holder.lnStatus.setBackgroundColor(mContext!!.resources.getColor(R.color.vermelho))
                        holder.lnStatus.invalidate()
                    }
                }
            } else {
                holder.lnStatus.setBackgroundColor(mContext!!.resources.getColor(R.color.vermelho))
                holder.lnStatus.invalidate()
            }

            //holder.mRow.setOnClickListener(mClick(i));
        } catch (err: Exception) {
            if (mContext != null) {
                LogTrace.logCatch(mContext, mContext!!.javaClass, err, true)
            }
        }
    }


    override fun getItemCount(): Int {
        return mLista!!.size
    }
}