package com.spotpromo.spotchat.dao

import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import br.com.spotpromo.faber_voltas_aulas.regras.model.DAOHelper
import com.spotpromo.spotchat.model.Contato
import java.util.*

class ContatoDAO @Throws(java.lang.Exception::class)
constructor(db: SQLiteDatabase) : DAOHelper(db) {
    private var dbb: SQLiteDatabase? = null

    init {
        this.dbb = db
    }




    /**
     * DELETAR TUDO
     *
     * @return
     */
    @Throws(Exception::class)
    fun deletar() {
        val tabelas = arrayOf(
            "TB_Contatos"
        )
        for (i in tabelas.indices) {
            //Monta Query
            val query = String.format("DELETE FROM %s", tabelas[i])
            this.executaNoQuery(query)
        }
    }

    @Throws(Exception::class)
    fun select(codPessoa: String?): ArrayList<Contato>? {
        var cursor: Cursor? = null
        val sbQuery = StringBuilder()
        sbQuery.append("SELECT DISTINCT * FROM TB_Contatos  where codPessoa <> ? order by nome ")
        cursor = codPessoa?.let { this.executaQuery(sbQuery.toString(), it) }
        val mLista: ArrayList<Contato> = ArrayList<Contato>()
        if (cursor != null && cursor.moveToFirst()) {
            do {
                val contato = Contato()
                contato.setCodPessoa(cursor.getInt(cursor.getColumnIndex("codPessoa")))
                contato.setNome(cursor.getString(cursor.getColumnIndex("nome")))
                contato.setRota(cursor.getString(cursor.getColumnIndex("rota")))
                contato.setFoto(cursor.getString(cursor.getColumnIndex("foto")))
                mLista.add(contato)
            } while (cursor.moveToNext())
        }
        cursor?.close()
        return mLista
    }

}