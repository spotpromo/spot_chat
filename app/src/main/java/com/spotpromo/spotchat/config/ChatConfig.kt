package com.spotpromo.spotchat.config

object ChatConfig {
    var db_folder = ""
    var url_chat_photo = ""
    var url_push = ""
    var url_profile_photo = ""
    var url_default_chat = ""
    var package_id = ""
}