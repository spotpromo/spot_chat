package com.spotpromo.spotchat.utils.firebase

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference

object ConfiguracaoFirebase {

    private var database: DatabaseReference? = null
    private var auth: FirebaseAuth? = null
    private var storage: StorageReference? = null

    //retornar do Firebase
    fun getFirebaseDatabase(): DatabaseReference? {
        if (database == null) {
            val firebaseDatabase: FirebaseDatabase = FirebaseDatabase.getInstance()
            //firebaseDatabase.setPersistenceEnabled(true);
            database = firebaseDatabase.getReference()
        }
        return database
    }

    //retornar instancia FirebaseAuth
    fun getFirebaseAutenticacao(): FirebaseAuth? {
        if (auth == null) {
            auth = FirebaseAuth.getInstance()
        }
        return auth
    }

    //retornar do Storage do Firebase
    fun getFirebaseStorage(): StorageReference? {
        if (storage == null) {
            storage = FirebaseStorage.getInstance().getReference()
        }
        return storage
    }
}