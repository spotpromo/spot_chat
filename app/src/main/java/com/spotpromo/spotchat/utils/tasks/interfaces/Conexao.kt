package com.spotpromo.spotchat.utils.tasks.interfaces

import android.content.Context
import com.spotpromo.spotchat.model.DadosJson
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import kotlin.jvm.Throws

class Conexao(val context: Context) : Retrofit_Client() {

    @Throws(Exception::class)
    fun sendFile(url: String?, body: RequestBody?): Call<DadosJson> {
        val dadosMobileClient: Mobile_Client = getClient(
            url!!,
            Mobile_Client::class.java
        ) as Mobile_Client
        return dadosMobileClient.sendFile(url!!, body!!)

        }

    fun file(url: String, body: RequestBody): Call<DadosJson> {
        val dados = getClient(url, Mobile_Client::class.java) as Mobile_Client
        return dados.sendFile(url, body)
    }

}