package com.spotpromo.spotchat.utils.tasks

import android.content.Context
import android.media.MediaMetadataRetriever
import android.net.Uri
import android.os.AsyncTask
import android.os.Environment
import android.util.Log
import br.com.spotpromo.nestle_dpa_novo.util.SupportImagem
import com.example.spotpix.util.FormataData
import com.iceteck.silicompressorr.SiliCompressor
import com.spotpromo.spotchat.R
import com.spotpromo.spotchat.config.ChatConfig
import com.spotpromo.spotchat.controller.CallBack_Projeto
import com.spotpromo.spotchat.utils.APIError
import com.spotpromo.spotchat.utils.Loading
import com.spotpromo.spotchat.utils.LogTrace
import com.spotpromo.spotchat.utils.Util
import com.spotpromo.spotchat.utils.tasks.interfaces.Conexao
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File

class TaskFotoChat(context: Context) : AsyncTask<String, Void, Boolean>() {

    var mensagem: String? = null
    var dataCelular: String? = null
    var callback: CallBack_Projeto? = null
    var nomeFile: String? = null
    var context: Context

    init {
        callback = context as CallBack_Projeto
        this.context = context
    }


    override fun onPreExecute() {
        super.onPreExecute()
        Loading.hide()
        Loading.show(
            this.context,
            this.context.getResources().getString(R.string.enviando_arquivo_chat)
        )
    }

    override  fun doInBackground(vararg nomeArquivo: String): Boolean? {
        try {
            Thread.sleep(200)
            mensagem = nomeArquivo[1]
            Log.i("nomeArquivo", nomeArquivo[0].toString())
            if (nomeArquivo[0].isNotEmpty()) {
                var file = File(nomeArquivo[0])
                Log.i("nomeArquivo", file.absolutePath)
                if (!file.isFile) throw RuntimeException(context!!.resources.getString(R.string.erro_foto))
                if (Util.retornaMimeType(file, context)!!.contains("image")) file =
                    SupportImagem.ajustOrientationFile(file, 640, 480)
                var filePath: String? = ""
                filePath = if (nomeArquivo[0].contains(".mp4")) {
                    val fileComprimido = File(
                        context.getExternalFilesDir(null)
                            .toString() + File.separator + "SPOT_FOTOS" + File.separator + "chat"
                    )
                    // int file_size = Integer.parseInt(String.valueOf(fileComprimido.length() / 1024));

                    //new VideoCompressAsyncTask(context).execute(file.getAbsolutePath().toString(),fileComprimido.getAbsolutePath().toString());
                    val file_size: Long = Util.getFolderSize(file)
                    Log.i("file_size", file_size.toString() + "")
                    if (file_size > 3000000) {
                        val retriever = MediaMetadataRetriever()
                        Log.i("path1", file.path)
                        Log.i("path2", file.absolutePath)
                        retriever.setDataSource(file.path)
                        val width =
                            Integer.valueOf(retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_WIDTH))
                        val height =
                            Integer.valueOf(retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_HEIGHT))
                        retriever.release()
                        SiliCompressor.with(context).compressVideo(
                            Uri.fromFile(file), fileComprimido.absolutePath, width,
                            height,
                            1500000
                        )

                        //Files.move(file, fileNovo.resolveSibling("text1.txt"), StandardCopyOption.REPLACE_EXISTING);
                        //Log.i("file_compromido", filePath);
                    } else {
                        file.absolutePath
                    }
                } else {
                    file.absolutePath
                }
                file = File(filePath)
                nomeFile = file.name
                dataCelular = FormataData.retornaDataFormat(FormataData.Numerico)
                val builder = MultipartBody.Builder()
                builder.setType(MultipartBody.FORM)
                builder.addFormDataPart("data_celular", dataCelular)
                builder.addFormDataPart(
                    "files[]", Util.retonarNomeFoto(file.absolutePath), RequestBody.create(
                        MediaType.parse(Util.retornaMimeType(file, context)), file
                    )
                )
                var request = Conexao(context).sendFile(
                    ChatConfig.url_chat_photo,
                    builder.build()
                )
                //  var response = request.execute()

                val response = request.execute()
                if (!response.isSuccessful) throw RuntimeException(
                    APIError.getError(
                        response.code(),
                        context
                    )
                )
                if (response.body()!!.mMensagem == null) throw RuntimeException(
                    context!!.resources.getString(
                        R.string.erro_dados
                    )
                )
                val mensagemJson = response.body()!!.mMensagem
                if (mensagemJson!!.status!! < 1) throw RuntimeException(
                    mensagemJson.mensagem
                )
            }
            return true
        } catch (err: Exception) {
            mensagem = LogTrace.logCatch(this.context, this.context.javaClass, err, false)
        }
        return false
    }

    override fun onPostExecute( aBoolean: Boolean?) {
        super.onPostExecute(aBoolean)
        Loading.hide()
        if (aBoolean != null) {
            callback!!.onRetorno(aBoolean, dataCelular, mensagem, nomeFile)
        }
    }
}