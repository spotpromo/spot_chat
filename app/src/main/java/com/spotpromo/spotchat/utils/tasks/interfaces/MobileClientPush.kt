package com.spotpromo.spotchat.utils.tasks.interfaces

import com.google.gson.JsonElement
import com.spotpromo.spotchat.model.Push
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST

interface MobileClientPush {

    @Headers("Content-Type:application/json; charset=UTF-8")
    @POST("chat.php")
    fun getPush(@Body body: Push?): Call<JsonElement?>?
}