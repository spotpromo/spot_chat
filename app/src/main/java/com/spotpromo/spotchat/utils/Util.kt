package com.spotpromo.spotchat.utils

import android.app.Activity
import android.content.ContentUris
import android.content.Context
import android.content.Intent
import android.content.res.ColorStateList
import android.database.Cursor
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.Typeface
import android.net.Uri
import android.os.Build
import android.provider.DocumentsContract
import android.provider.MediaStore
import android.text.Spannable
import android.text.SpannableString
import android.text.SpannableStringBuilder
import android.text.TextPaint
import android.text.method.LinkMovementMethod
import android.text.style.BackgroundColorSpan
import android.text.style.ClickableSpan
import android.text.style.RelativeSizeSpan
import android.text.style.TextAppearanceSpan
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.MimeTypeMap
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.core.content.FileProvider
import com.ortiz.touchview.TouchImageView
import com.spotpromo.spotchat.BuildConfig
import com.spotpromo.spotchat.R
import com.spotpromo.spotchat.chat.activity.ChatActivity
import com.spotpromo.spotchat.config.ChatConfig
import java.io.File
import java.io.FileOutputStream
import java.io.InputStream
import java.io.OutputStream
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Pattern

object Util {

    /**
     * RETORNA PATH DA GALERIA
     *
     * @param uri
     * @param context
     * @return
     */
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    fun retornaPathImage(uri: Uri, context: Context): String? {
        // check here to KITKAT or new version
        val isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {

            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                val docId = DocumentsContract.getDocumentId(uri)
                val split = docId.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                val type = split[0]

                if ("primary".equals(type, ignoreCase = true)) {
                    return (context.getExternalFilesDir(null).toString() + "/"
                            + split[1])
                }
            } else if (isDownloadsDocument(uri)) {

                val id = DocumentsContract.getDocumentId(uri)
                val contentUri = ContentUris.withAppendedId(
                    Uri.parse("content://downloads/public_downloads"),
                    java.lang.Long.valueOf(id)
                )

                return getDataColumn(context, contentUri, null, null)
            } else if (isMediaDocument(uri)) {
                val docId = DocumentsContract.getDocumentId(uri)
                val split = docId.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                val type = split[0]

                var contentUri: Uri? = null
                if ("image" == type) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                } else if ("video" == type) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI
                } else if ("audio" == type) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
                }

                val selection = "_id=?"
                val selectionArgs = arrayOf(split[1])

                return getDataColumn(
                    context, contentUri, selection,
                    selectionArgs
                )
            }// MediaProvider
            // DownloadsProvider
        } else if ("content".equals(uri.scheme!!, ignoreCase = true)) {

            // Return the remote address
            return if (isGooglePhotosUri(uri)) uri.lastPathSegment else getDataColumn(
                context,
                uri,
                null,
                null
            )

        } else if ("file".equals(uri.scheme!!, ignoreCase = true)) {
            return uri.path
        }// File
        // MediaStore (and general)

        return null
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    fun isExternalStorageDocument(uri: Uri): Boolean {
        return "com.android.externalstorage.documents" == uri
            .authority
    }


    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    fun isDownloadsDocument(uri: Uri): Boolean {
        return "com.android.providers.downloads.documents" == uri
            .authority
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    fun isMediaDocument(uri: Uri): Boolean {
        return "com.android.providers.media.documents" == uri
            .authority
    }

    /**
     * Get the value of the data column for this Uri. This is useful for
     * MediaStore Uris, and other file-based ContentProviders.
     *
     * @param context       The context.
     * @param uri           The Uri to query.
     * @param selection     (Optional) Filter used in the query.
     * @param selectionArgs (Optional) Selection arguments used in the query.
     * @return The value of the _data column, which is typically a file path.
     */
    fun getDataColumn(
        context: Context, uri: Uri?,
        selection: String?, selectionArgs: Array<String>?
    ): String? {

        var cursor: Cursor? = null
        val column = "_data"
        val projection = arrayOf(column)

        try {
            cursor = context.contentResolver.query(
                uri!!, projection,
                selection, selectionArgs, null
            )
            if (cursor != null && cursor.moveToFirst()) {
                val index = cursor.getColumnIndexOrThrow(column)
                return cursor.getString(index)
            }
        } finally {
            cursor?.close()
        }
        return null
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is Google Photos.
     */
    fun isGooglePhotosUri(uri: Uri): Boolean {
        return "com.google.android.apps.photos.content" == uri
            .authority
    }

    /**
     * NAPER
     */
    fun retonarNomeFoto(caminhoFoto: String): String {
        val foto = caminhoFoto.split("/")

        return foto[foto.size - 1]
    }

    /**
     * ABRE GALERIA
     *
     * @param activity
     * @return
     */
    fun abrirGaleriaChat(activity: Activity, retorno: Int?) {
        try {
            val intent = Intent()
            intent.type = "*/*"
            intent.action = Intent.ACTION_GET_CONTENT
            activity.startActivityForResult(
                Intent.createChooser(intent, "Select the photo"),
                retorno!!
            )
        } catch (e: java.lang.Exception) {
            Log.v("TAG", "Não é possível selecionar a foto.")
        }
    }

    @Throws(java.lang.Exception::class)
    fun retornaCaminhoFotoURINew(
        uriAtual: Uri?,
        codRoteiro: Int,
        nomefoto: String?,
        context: Context
    ): String? {
        var retorna_novo_caminho_ou_antigo = ""
        val sdcard = context.getExternalFilesDir(null)

        /** PASTA PROJETO  */
        val pasta_projeto = File(
            sdcard!!.absolutePath + File.separator + ChatConfig.db_folder
        )
        if (!pasta_projeto.isDirectory) pasta_projeto.mkdirs()
        /** PASTA SPOT FOTOS  */
        val pasta_spot = File(
            pasta_projeto.absolutePath + File.separator + context.resources
                .getString(R.string.name_directory_fotos)
        )
        if (!pasta_spot.isDirectory) pasta_spot.mkdirs()
        val pasta_roteiro =
            File(pasta_spot.absolutePath + File.separator + codRoteiro)
        if (!pasta_roteiro.isDirectory) pasta_roteiro.mkdirs()
        val stringi_file_copy: String = CriarFileTemporarioStringRoteiro(
            pasta_roteiro.absolutePath,
            nomefoto!!,
            codRoteiro
        )
        val file_copy = File(stringi_file_copy)

        /** COPIAR ARQUIVO PARA NOVA PASTA  */
        var `in`: InputStream? = null
        var out: OutputStream? = null
        try {
            `in` = context.contentResolver.openInputStream(uriAtual!!)
            out = FileOutputStream(file_copy)

            val buf = ByteArray(1024)
            var len: Int
            while (`in`!!.read(buf).also { len = it } > 0) {
                out.write(buf, 0, len)
            }

            if (file_copy.isFile) retorna_novo_caminho_ou_antigo = file_copy.absolutePath
        } finally {
            `in`?.close()
            out?.close()
        }

        val file_movido = File(retorna_novo_caminho_ou_antigo)
        if (!file_movido.isFile)
            throw Exception("A foto não foi atualizada, por favor tente novamente")

        return retorna_novo_caminho_ou_antigo

    }

    @Throws(java.lang.Exception::class)
    fun CriarFileTemporarioStringRoteiro(
        diretorio: String,
        nomefoto: String,
        codRoteiro: Int
    ): String {
        val timeStamp =
            SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault())
                .format(Date())
        val mediaFile: File
        mediaFile =
            File(diretorio + File.separator + nomefoto + "_" + codRoteiro + "_" + timeStamp + ".jpg")
        return mediaFile.absolutePath
    }

    fun uTCToLocal(
        dateFomratIn: String?,
        dataFormatOut: String?,
        datesToConvert: String?
    ): String? {
        var dateToReturn = datesToConvert
        val sdf = SimpleDateFormat(dateFomratIn)
        sdf.timeZone = TimeZone.getTimeZone("UTC")
        var gmt: Date? = null
        val sdfOutPutToSend = SimpleDateFormat(dataFormatOut)
        sdfOutPutToSend.timeZone = TimeZone.getDefault()
        try {
            gmt = sdf.parse(datesToConvert)
            dateToReturn = sdfOutPutToSend.format(gmt)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return dateToReturn
    }

    fun setTags(pTextView: TextView, pTagString: String) {
        val string = SpannableString(pTagString)
        var start = -1
        var i = 0
        while (i < pTagString.length) {
            if (pTagString[i] == '#') {
                start = i
            } else if (pTagString[i] == ' ' || pTagString[i] == '\n' || i == pTagString.length - 1 && start != -1) {
                if (start != -1) {
                    if (i == pTagString.length - 1) {
                        i++ // case for if hash is last word and there is no
                        // space after word
                    }
                    val tag = pTagString.substring(start, i)
                    string.setSpan(object : ClickableSpan() {
                        override fun onClick(widget: View) {
                            Log.d("Hash", String.format("Clicked %s!", tag))
                        }

                        override fun updateDrawState(ds: TextPaint) {
                            // link color
                            ds.color = Color.parseColor("#3361FF")
                            ds.isUnderlineText = false
                        }
                    }, start, i, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
                    start = -1
                }
            }
            i++
        }
        pTextView.movementMethod = LinkMovementMethod.getInstance()
        pTextView.text = string
    }

    fun setSearchTextHighlightSimpleSpannable(
        textView: TextView,
        fullText: String?,
        searchText: String?
    ) {
        if (null != searchText && !searchText.isEmpty()) {
            val wordSpan = SpannableStringBuilder(fullText)
            val p = Pattern.compile(searchText, Pattern.CASE_INSENSITIVE)
            val m = p.matcher(fullText)
            while (m.find()) {
                val wordStart = m.start()
                val wordEnd = m.end()

                // Now highlight based on the word boundaries
                val redColor = ColorStateList(arrayOf(intArrayOf()), intArrayOf(-0x5ef6ff))
                val highlightSpan = TextAppearanceSpan(null, Typeface.BOLD, -1, redColor, null)
                wordSpan.setSpan(
                    highlightSpan,
                    wordStart,
                    wordEnd,
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
                )
                wordSpan.setSpan(
                    BackgroundColorSpan(-0x300b8),
                    wordStart,
                    wordEnd,
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
                )
                wordSpan.setSpan(
                    RelativeSizeSpan(1.25f),
                    wordStart,
                    wordEnd,
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
                )
            }
            textView.setText(wordSpan, TextView.BufferType.SPANNABLE)
        } else {
            textView.text = fullText
        }
    }

    @Throws(Exception::class)
    fun showImageGalleryChat(
        context: Context?,
        imagemBitmap: Bitmap?,
        vB: View?,
        activity: ChatActivity
    ) { // final View vB
        val params = LinearLayout.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.MATCH_PARENT,
            1F
        )
        val mDialog: androidx.appcompat.app.AlertDialog
        val builder = androidx.appcompat.app.AlertDialog.Builder(activity, R.style.MyAlertDialogStyleDialog)
        val inflater: LayoutInflater = activity.layoutInflater
        val dialogView = inflater.inflate(R.layout.dialog_show_image, null)
        val timageView = dialogView.findViewById<View>(R.id.timageproduto) as TouchImageView
        val fechar = dialogView.findViewById<View>(R.id.fechar) as ImageView
        timageView.setImageBitmap(imagemBitmap)
        //timageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        //dialogView.setLayoutParams(params);
        builder.setView(dialogView)
        mDialog = builder.create()
        mDialog.show()
        fechar.setOnClickListener {
            if (vB != null) vB.isClickable = true
            if (mDialog != null && mDialog.isShowing) mDialog.dismiss()
        }
    }

    @Throws(Exception::class)
    fun retornaMimeType(file: File, context: Context): String? {

        var uri: Uri? = null

        uri = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            FileProvider.getUriForFile(context, ChatConfig.package_id + ".provider", file)
        } else {
            Uri.fromFile(file)
        }

        val fileExtension = MimeTypeMap.getFileExtensionFromUrl(uri!!.toString())

        return MimeTypeMap.getSingleton().getMimeTypeFromExtension(fileExtension)
    }

    fun getFolderSize(file: File): Long {
        var size: Long = 0
        if (file.isDirectory) {
            for (child in file.listFiles()) {
                size += getFolderSize(child)
            }
        } else {
            size = file.length()
        }
        return size
    }
}