package com.spotpromo.spotchat.utils.tasks

import android.content.Context
import android.os.AsyncTask
import br.com.spotpromo.nestle_dpa_novo.util.SupportImagem
import com.example.spotpix.util.FormataData
import com.spotpromo.spotchat.R
import com.spotpromo.spotchat.config.ChatConfig
import com.spotpromo.spotchat.controller.CallBack_Projeto
import com.spotpromo.spotchat.model.DadosJson
import com.spotpromo.spotchat.utils.APIError
import com.spotpromo.spotchat.utils.Loading
import com.spotpromo.spotchat.utils.LogTrace
import com.spotpromo.spotchat.utils.Util
import com.spotpromo.spotchat.utils.tasks.interfaces.Conexao
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Response
import java.io.File

class TaskFotoGrupo(val context: Context) :  AsyncTask<String, Void, Boolean>() {

    var mensagem: String? = null
    var dataCelular: String? = null
    var callback: CallBack_Projeto? = null

    override fun onPreExecute() {
        super.onPreExecute()
        Loading.hide()
        Loading.show(
            context!!,
            context!!.getString(R.string.enviando_arquivo_chat)
        )
    }

    override fun doInBackground(vararg nomeArquivo: String): Boolean? {
        try {
            Thread.sleep(200)
            if (nomeArquivo[0].isNotEmpty()) {
                var file = File(nomeArquivo[0])
                if (!file.isFile) throw RuntimeException(context!!.resources.getString(R.string.erro_foto))
                if (Util.retornaMimeType(file, context!!)!!.contains("image")) file =
                    SupportImagem.ajustOrientationFile(file, 640, 480)
                if (nomeArquivo[0].contains(".mp4")) {
                    // File fileComprimido = new File(Environment.getExternalStorageDirectory()+File.separator+"SGDM_LG_USA"+File.separator+"chat");
                    //new VideoCompressAsyncTask(context).execute(file.getAbsolutePath().toString(),fileComprimido.getAbsolutePath().toString());
                }
                dataCelular = FormataData.retornaDataFormat(FormataData.Numerico)
                /** ENVIAR O ARQUIVO .DB  */
                val builder = MultipartBody.Builder()
                builder.setType(MultipartBody.FORM)
                builder.addFormDataPart("data_celular", dataCelular)
                builder.addFormDataPart(
                    "files[]", Util.retonarNomeFoto(file.absolutePath), RequestBody.create(
                        MediaType.parse(context?.let { Util.retornaMimeType(file, it) }), file
                    )
                )
                val request: Call<DadosJson> = Conexao(context).file(
                    ChatConfig.url_chat_photo,
                    builder.build()
                )
                val response: Response<DadosJson> = request.execute()
                if (!response.isSuccessful) throw RuntimeException(
                    APIError.getError(
                        response.code(),
                        context!!
                    )
                )
                if (response.body()!!.mMensagem == null) throw RuntimeException(
                    context!!.resources.getString(
                        R.string.erro_dados
                    )
                )
                val mensagemJson = response.body()!!.mMensagem
                if (mensagemJson!!.status!! != 1) throw RuntimeException(
                    mensagemJson.mensagem!!
                )
            }
            return true
        } catch (err: Exception) {
            mensagem = LogTrace.logCatch(context!!, this.javaClass, err, false)
        }
        return false
    }

    override fun onPostExecute(aBoolean: Boolean?) {
        super.onPostExecute(aBoolean)
        Loading.hide()
        aBoolean?.let { callback!!.onRetornoFotoGrupo(aBolean = it, data = dataCelular, mensagem = mensagem) }
    }
}