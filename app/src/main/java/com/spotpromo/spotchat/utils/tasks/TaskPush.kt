package com.spotpromo.spotchat.utils.tasks

import android.content.Context
import android.os.AsyncTask
import android.util.Log
import com.google.gson.JsonElement
import com.spotpromo.spotchat.config.ChatConfig
import com.spotpromo.spotchat.model.Push
import com.spotpromo.spotchat.utils.tasks.interfaces.MobileClientPush
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class TaskPush(val context: Context) : AsyncTask<Push, Void, Boolean>() {

    val mensagem: String? = null


    override fun onPreExecute() {
        super.onPreExecute()
        /* Loading.hide();
        Loading.show(this.context, this.context.getResources().getString(R.string.login_loading_validando));*/
    }

    override fun doInBackground(vararg pushes: Push?): Boolean? {
        try {
            Thread.sleep(200)

            // Log.i("json",pushes[0].getUsuarios().toString());
            /*  Call<PushResposta> request = new Conexao(this.context).getPush(this.context.getResources().getString(R.string.url_push), Util.retornaHashJson(
                    String.format("dadosJson=%1$s", strings[0])));


            Response<PushResposta> response = request.execute();

            / ** VERIFICA SE A CONEXAO FOI SUCESSO
            if (!response.isSuccessful())
                throw new RuntimeException(APIError.getError(response.code(), this.context));

            / ** VERIFICA SE O OBJETO MENSAGEM NAO ESTA NULO
            if (response.body().getError() == null)
                throw new RuntimeException(this.context.getResources().getString(R.string.erro_dados));*/

            /*MensagemJson mensagemJson = response.body().getError();

            if (!mensagemJson.getStatus().trim().equals("OK"))
                throw new RuntimeException(mensagemJson.getMensagem());*/
            val retrofit: Retrofit = Retrofit.Builder()
                .baseUrl(
                    ChatConfig.url_push
                ) //.addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build()
            val apiInterface = retrofit.create(MobileClientPush::class.java)
            // prepare call in Retrofit 2.0
            val pushCall: Call<JsonElement?>? = apiInterface.getPush(pushes[0])
            if (pushCall != null) {
                pushCall.enqueue(object : Callback<JsonElement?> {
                    override fun onResponse(
                        call: Call<JsonElement?>,
                        response: Response<JsonElement?>
                    ) {
                        if (response.isSuccessful) {
                            return
                        }
                    }

                    override fun onFailure(call: Call<JsonElement?>, t: Throwable) {
                        Log.i("deu merda", "deu merda" + t.message)
                    }
                })
            }
            return true
            // userCall.enqueue(this);
        } catch (e: InterruptedException) {
            e.printStackTrace()
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return false
    }


    override fun onPostExecute(aBoolean: Boolean?) {
        super.onPostExecute(aBoolean)
        // Loading.hide();
    }
}