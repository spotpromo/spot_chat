package com.spotpromo.spotchat.model

import com.google.firebase.database.Exclude
import com.google.firebase.database.PropertyName
import com.spotpromo.spotchat.utils.firebase.ConfiguracaoFirebase
import java.io.Serializable

data class Mensagem(
    @PropertyName("id")
    var id: String? = null,
    @PropertyName("idUsuario")
    var idUsuario: String? = null,
    @PropertyName("mensagem")
    var mensagem: String? = null,
    @PropertyName("imagem")
    var imagem: String? = null,
    @PropertyName("nome")
    var nome: String? = null,
    @PropertyName("nomeArquivo")
    var nomeArquivo: String? = null,
    @PropertyName("lido")
    var lido: Int? = null,
    @PropertyName("idGrupoMensagem")
    var idGrupoMensagem: String? = null,
    @PropertyName("data")
    var data: String? = null,
    @Exclude
    var comQuemEstouFalando: String? = null,
    @Exclude
    var idMensagemEncontrada: String? = null,
    @PropertyName("flExcluida")
    var flExcluida: Int? = null,
    @PropertyName("msgAdmin")
    var msgAdmin: String? = null,
    @PropertyName("flImagem")
    var flImagem: Boolean =  false,
    @PropertyName("isSearch")
    var isSearch: Boolean =  false,
    @PropertyName("posicao")
    var posicao: Int? = null): Serializable {

    init{
        val databaseReference  = ConfiguracaoFirebase.getFirebaseDatabase()
        val grupoRef = databaseReference!!.child("mensagens")
        val idMensagem = grupoRef.push().key
        id = idMensagem
        nome = ""
    }

    fun salvar(idRementente: String?, idDestinatario: String?, msg: Mensagem?) {
        val database = ConfiguracaoFirebase.getFirebaseDatabase()
        val mensagemRef = database!!.child("mensagens")
        mensagemRef.child(idDestinatario!!)
            .child(idRementente!!)
            .child(id!!)
            .setValue(msg)
    }


}