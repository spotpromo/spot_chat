package com.spotpromo.spotchat.model

import com.spotpromo.spotchat.utils.firebase.ConfiguracaoFirebase


class GrupoMensagem {

    private var idGrupoMensagem: String? = null
    private var mensagem: String? = null
    private var imagem: String? = null
    private var lido: String? = null
    private var idRemetente: Int? = null
    private var data: String? = null
    private var flImagem = false
    private var listaContatos: List<Contato?>? = null

    fun GrupoMensagem() {
        val databaseReference = ConfiguracaoFirebase.getFirebaseDatabase()
        val grupoRef = databaseReference!!.child("grupoMensagem")
        val idGrupoMensagem = grupoRef.push().key
        setIdGrupoMensagem(idGrupoMensagem)
    }

    fun salvar(idGrupo: String?, grupoMensagem: GrupoMensagem?) {
        val database = ConfiguracaoFirebase.getFirebaseDatabase()
        val conversaRef = database!!.child("grupoMensagem")
        conversaRef.child(idGrupo!!)
            .push()
            .setValue(grupoMensagem)
    }

    fun getData(): String? {
        return data
    }

    fun setData(data: String?) {
        this.data = data
    }

    fun getIdRemetente(): Int? {
        return idRemetente
    }

    fun setIdRemetente(idRemetente: Int?) {
        this.idRemetente = idRemetente
    }

    fun getMensagem(): String? {
        return mensagem
    }

    fun setMensagem(mensagem: String?) {
        this.mensagem = mensagem
    }

    fun getImagem(): String? {
        return imagem
    }

    fun setImagem(imagem: String?) {
        this.imagem = imagem
    }

    fun getIdGrupoMensagem(): String? {
        return idGrupoMensagem
    }

    fun setIdGrupoMensagem(idGrupoMensagem: String?) {
        this.idGrupoMensagem = idGrupoMensagem
    }

    fun getLido(): String? {
        return lido
    }

    fun setLido(lido: String?) {
        this.lido = lido
    }

    fun getListaContatos(): List<Contato?>? {
        return listaContatos
    }

    fun setListaContatos(listaContatos: List<Contato?>?) {
        this.listaContatos = listaContatos
    }

    fun isFlImagem(): Boolean {
        return flImagem
    }

    fun setFlImagem(flImagem: Boolean) {
        this.flImagem = flImagem
    }
}