package com.spotpromo.spotchat.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class Contato : Serializable {

    private var id: Int? = null

    @SerializedName("codPessoa")
    private var codPessoa: Int? = null

    @SerializedName("nomePessoa")
    private var nome: String? = null

    @SerializedName("desRota")
    private var rota: String? = null

    @SerializedName("foto")
    private var foto: String? = null

    @SerializedName("lido")
    private var lido: Int? = null



    fun getLido(): Int? {
        return lido
    }

    fun setLido(lido: Int?) {
        this.lido = lido
    }

    fun getFoto(): String? {
        return foto
    }

    fun setFoto(foto: String?) {
        this.foto = foto
    }

    fun getId(): Int? {
        return id
    }

    fun setId(id: Int?) {
        this.id = id
    }

    fun getCodPessoa(): Int? {
        return codPessoa
    }

    fun setCodPessoa(codPessoa: Int?) {
        this.codPessoa = codPessoa
    }

    fun getNome(): String? {
        return nome
    }

    fun setNome(nome: String?) {
        this.nome = nome
    }

    fun getRota(): String? {
        return rota
    }

    fun setRota(rota: String?) {
        this.rota = rota
    }
}