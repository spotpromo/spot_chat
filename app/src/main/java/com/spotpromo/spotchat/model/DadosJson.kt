package com.spotpromo.spotchat.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class DadosJson : Serializable {

    @SerializedName("MENSAGEM")
    var mMensagem: MensagemJson? = null

}