package com.spotpromo.spotchat.model

import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.PropertyName
import com.spotpromo.spotchat.utils.firebase.ConfiguracaoFirebase
import java.io.Serializable

data class Grupo(
    @PropertyName("id")
    var id: String? = null,
    @PropertyName("nome")
    var nome: String? = null,
    @PropertyName("foto")
    var foto: String? = null,
    @PropertyName("codPessoa")
    var codPessoa: Int? = null,
    @PropertyName("bu")
    var bu: Int? = null,
    @PropertyName("usuarios")
    var usuarios: List<Contato>? = null) : Serializable{



    init {
        val databaseReference  = ConfiguracaoFirebase.getFirebaseDatabase()
        val grupoRef: DatabaseReference = databaseReference!!.child("grupos")
        val idGrupoFirebase: String = grupoRef.push().key!!
        id = idGrupoFirebase
    }

    fun salvar() {
        val databaseReference = ConfiguracaoFirebase.getFirebaseDatabase()
        val grupoRef = databaseReference!!.child("grupos")
        grupoRef.child(id.toString()).setValue(this)
        for (contato in usuarios!!) {
            val idRemetente: String = contato.getCodPessoa().toString()
            val idDestinatario = id
            val conversa = Conversa()
            conversa.idRemetente = idRemetente
            conversa.idDestinatario = idDestinatario
            conversa.ultimaMensagem = ""
            conversa.isGroup ="true"
            conversa.grupo = this
            conversa.salvar()
        }
    }

}