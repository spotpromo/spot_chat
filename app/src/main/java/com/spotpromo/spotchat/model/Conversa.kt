package com.spotpromo.spotchat.model

import com.google.firebase.database.Exclude
import com.google.firebase.database.PropertyName
import com.spotpromo.spotchat.utils.firebase.ConfiguracaoFirebase

data class Conversa(
    @PropertyName("idRemetente")
    var idRemetente: String? = null,
    @PropertyName("isGroup")
    var isGroup: String? = null,
    @PropertyName("idDestinatario")
    var idDestinatario: String? = null,
    @PropertyName("ultimaMensagem")
    var ultimaMensagem: String? = null,
    @PropertyName("usuarioExibicao")
    var usuarioExibicao: Contato? = null,
    @PropertyName("grupo")
    var grupo: Grupo? = null,
    @PropertyName("lido")
    var lido: Int? = null,
    @PropertyName("flImagem")
    var flImagem: Boolean? = null,
    @Exclude
    var chaveMensagem: String? = null) {

    fun salvar() {
        val database  = ConfiguracaoFirebase.getFirebaseDatabase()
        val conversaRef = database!!.child("conversas")
        conversaRef.child(idRemetente!!)
            .child(idDestinatario!!)
            .setValue(this)
    }

}