package com.spotpromo.spotchat.model

import android.app.Activity
import android.content.Context
import com.google.gson.Gson
import com.google.gson.annotations.SerializedName
import java.io.Serializable
import java.lang.Exception

class Login : Serializable {

    var PREFS_LOGIN: String = "prefs_usuario";
    var PREFS_LOGIN_DADOS: String = "prefs_usuario_dados";

    @SerializedName("codPessoa")
    var codPessoa: Int? = null
    @SerializedName("nomPessoa")
    var nomPessoa: String? = null
    @SerializedName("codRota")
    var codRota: Int? = null
    @SerializedName("desRota")
    var desRota: String? = null
    @SerializedName("codBU")
    var codBU: Int? = null
    @SerializedName("desBU")
    var desBU: String? = null
    @SerializedName("codSubBU")
    var codSubBU: Int? = null
    @SerializedName("desSubBU")
    var desSubBU: String? = null
    @SerializedName("codPerfil")
    var codPerfil: Int? = null
    @SerializedName("desPerfil")
    var desPerfil: String? = null
    @SerializedName("telefone")
    var telefone: String? = null
    @SerializedName("email")
    var email: String? = null
    @SerializedName("fotoPerfil")
    var fotoPerfil: String? = null
    @SerializedName("endereco")
    var endereco: String? = null
    @SerializedName("cidade")
    var cidade: String? = null
    @SerializedName("usuarioMobile")
    var usuario: String? = null
    @SerializedName("senhaMobile")
    var senha: String? = null

    var modificado : Int? = null
    /**
     * RETORAR LOGIN GRAVADO
     */

    fun retornar(context: Context): Login? {
        val prefs = context.getSharedPreferences(PREFS_LOGIN, Context.MODE_PRIVATE)

        val json = prefs.getString(PREFS_LOGIN_DADOS, "")
        val login = Gson().fromJson(json, Login::class.java)

        return login
    }

    @Throws(Exception::class)
    fun iniciar(context: Context, login: Login, direcionar: Boolean) {
        val prefs = context.getSharedPreferences(PREFS_LOGIN, Context.MODE_PRIVATE)
        val json = prefs.getString(PREFS_LOGIN_DADOS, "")
        val loginJson = Gson().fromJson(json, Login::class.java)

        if (loginJson != null)
            prefs.edit().clear().apply()

        val editor = prefs.edit()
        editor.putString(PREFS_LOGIN_DADOS, Gson().toJson(login))
        editor.apply()

        if (direcionar)
            (context as Activity).finish()
    }

    @Throws(Exception::class)
    fun deslogar(context: Context, redirecionar: Boolean) {
        val prefs = context.getSharedPreferences(PREFS_LOGIN, Context.MODE_PRIVATE)
        prefs.edit().clear().apply()

        if (redirecionar)
            (context as Activity).finish()
    }

}