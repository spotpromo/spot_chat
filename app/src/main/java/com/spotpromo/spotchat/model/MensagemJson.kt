package com.spotpromo.spotchat.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class MensagemJson : Serializable {
    @SerializedName("status")
    var status : Int? = null
    @SerializedName("descricao")
    var mensagem : String? = null
    @SerializedName("foto")
    var foto : String? = null
}