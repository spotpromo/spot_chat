package com.spotpromo.spotchat.chat.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.spotpromo.spotchat.R
import com.spotpromo.spotchat.model.Mensagem
import de.hdodenhof.circleimageview.CircleImageView

class ConversasMensagemAdapter(val lista: List<Mensagem?>, val context: Context) : RecyclerView.Adapter<ConversasMensagemAdapter.MyViewHolder>() {

    fun getConversas(): List<Mensagem?>? {
        return this.lista
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemLista: View = LayoutInflater.from(parent.context)
            .inflate(R.layout.adapter_conversas, parent, false)
        return MyViewHolder(itemLista)
    }

    override  fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val mensagem: Mensagem = lista!![position]!!
        try {
            holder.nome.text = mensagem.nome
            holder.ultimaMensagem.text = mensagem.mensagem
            holder.foto.visibility = View.GONE

        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun getItemCount(): Int {
        return lista!!.size
    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var foto: CircleImageView = itemView.findViewById(R.id.imageViewContatoFoto)
        var nome: TextView = itemView.findViewById(R.id.textContatoNome)
        var ultimaMensagem: TextView = itemView.findViewById(R.id.textViewContatoEmail)
        var novaMensagem: ImageView = itemView.findViewById(R.id.imageNovaMensagem)

    }
}