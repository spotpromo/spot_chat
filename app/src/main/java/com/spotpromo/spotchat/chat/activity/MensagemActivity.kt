package com.spotpromo.spotchat.chat.activity

import android.os.Build
import android.os.Bundle
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.database.ChildEventListener
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.spotpromo.spotchat.R
import com.spotpromo.spotchat.controller.CallBack_Projeto
import com.spotpromo.spotchat.model.Contato
import com.spotpromo.spotchat.model.Mensagem
import com.spotpromo.spotchat.recycler.RecycleListaMensagem
import com.spotpromo.spotchat.utils.firebase.ConfiguracaoFirebase
import java.util.*

class MensagemActivity  : AppCompatActivity(), CallBack_Projeto {


    var recycleListaMensagem: RecycleListaMensagem? = null
    var listaContatos: ArrayList<Contato?>? = null

    var database: DatabaseReference? = null
    var grupoMensagensRef: DatabaseReference? = null
    var childEventListenerContato: ChildEventListener? = null
    var mensagem: Mensagem? = null
    var nomeGrupo: String? = null
    var mKeys: List<String?>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_activity_lista_generic_branco)
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        toolbar.title = "Message Details"
        setSupportActionBar(toolbar)

        database = ConfiguracaoFirebase.getFirebaseDatabase()

        val bundle: Bundle? = intent.extras

        if (bundle != null) mensagem = bundle.getSerializable("mensagem") as Mensagem?
        nomeGrupo = bundle!!.getString("nomeGrupo")
        val recycle: RecyclerView = findViewById(R.id.recycle)
        recycle.setHasFixedSize(true)
        val mLayoutManager: RecyclerView.LayoutManager = LinearLayoutManager(this@MensagemActivity)
        recycle.layoutManager = mLayoutManager
        recycle.addItemDecoration(
            DividerItemDecoration(
                this@MensagemActivity,
                DividerItemDecoration.VERTICAL
            )
        )
        mKeys = ArrayList()
        listaContatos = ArrayList<Contato?>()
        recycleListaMensagem = RecycleListaMensagem(this@MensagemActivity, listaContatos!!)
        recycle.adapter = recycleListaMensagem
        grupoMensagensRef = mensagem!!.idGrupoMensagem?.let {
            database!!.child("grupoMensagem")
                .child(nomeGrupo!!)
                .child(it)
                .child("listaContatos")
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onStart() {
        super.onStart()
        recuperarConversas()
    }

    override fun onRetornoFotoGrupo(aBolean: Boolean, data: String?, mensagem: String?) {
        TODO("Not yet implemented")
    }

    override fun onRetorno(mensagem: Mensagem) {
        TODO("Not yet implemented")
    }

    override fun onRetorno(aBolean: Boolean, data: String?, mensagem: String?) {
        TODO("Not yet implemented")
    }

    override fun onRetorno(
        aBolean: Boolean,
        data: String?,
        mensagem: String?,
        nomeArquivo: String?
    ) {
        TODO("Not yet implemented")
    }

    override fun onRetorno(aBoolean: Boolean, mensagem: String) {
        TODO("Not yet implemented")
    }

    override fun onStop() {
        super.onStop()
        grupoMensagensRef!!.removeEventListener(childEventListenerContato!!)
    }


    fun recuperarConversas() {
        listaContatos!!.clear()
        childEventListenerContato =
            grupoMensagensRef!!.addChildEventListener(object : ChildEventListener {
                override fun onChildAdded(dataSnapshot: DataSnapshot, s: String?) {
                    val contato: Contato? = dataSnapshot.getValue(Contato::class.java)
                    listaContatos!!.add(contato)
                    recycleListaMensagem!!.notifyDataSetChanged()
                }

                override fun onChildChanged(dataSnapshot: DataSnapshot, s: String?) {
                    if (dataSnapshot != null) {
                        val contato: Contato? = dataSnapshot.getValue(Contato::class.java)
                        try {
                            val key = dataSnapshot.key
                            val index = mKeys!!.indexOf(key)
                            listaContatos!![index] = contato
                            recycleListaMensagem!!.notifyDataSetChanged()
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }
                }

                override fun onChildRemoved(dataSnapshot: DataSnapshot) {}
                override fun onChildMoved(dataSnapshot: DataSnapshot, s: String?) {}
                override fun onCancelled(databaseError: DatabaseError) {}
            })
    }
}