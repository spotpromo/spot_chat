package com.spotpromo.spotchat.chat.adapter

import android.content.Context
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.spotpromo.spotchat.R
import com.spotpromo.spotchat.model.Contato
import de.hdodenhof.circleimageview.CircleImageView

class GrupoSelecionadoAdapter(val listaMembros: List<Contato?>, val context: Context) : RecyclerView.Adapter<GrupoSelecionadoAdapter.MyViewHolder>()
{

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemLista: View = LayoutInflater.from(context)
            .inflate(R.layout.adapter_grupo_selecionado, parent, false)
        return MyViewHolder(itemLista)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val usuario: Contato = listaMembros[position]!!
        //validacao Grupo
        holder.nome.text = usuario.getNome()
        if (usuario.getFoto() != null) {
            Glide.with(context)
                .load(Uri.parse(usuario.getFoto()))
                .apply(RequestOptions().override(50, 50))
                .into(holder.foto)
        } else {
            holder.foto.setImageResource(R.drawable.padrao)
        }
    }

    override fun getItemCount(): Int {
        return listaMembros!!.size
    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var foto: CircleImageView
        var nome: TextView

        init {
            foto = itemView.findViewById(R.id.imageViewMembroSelecionado)
            nome = itemView.findViewById(R.id.textMembroSelecionado)
        }
    }
}