package com.spotpromo.spotchat.chat.activity

import android.content.DialogInterface
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.spotpromo.spotchat.chat.adapter.MensagemAdapter
import br.com.spotpromo.faber_voltas_aulas.regras.model.*
import com.spotpromo.spotchat.utils.tasks.TaskFotoChat
import com.bumptech.glide.Glide
import com.example.spotpix.util.FormataData
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.firebase.database.*
import com.google.firebase.database.ktx.getValue
import com.google.firebase.storage.StorageReference
import com.miguelcatalan.materialsearchview.MaterialSearchView
import com.miguelcatalan.materialsearchview.MaterialSearchView.SearchViewListener
import com.spotpromo.spotchat.controller.CallBack_Projeto
import com.spotpromo.spotchat.utils.Alerta
import com.spotpromo.spotchat.utils.Util
import com.spotpromo.spotchat.utils.firebase.ConfiguracaoFirebase
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.activity_chat.*
import kotlinx.android.synthetic.main.activity_chat.toolbar
import kotlinx.android.synthetic.main.content_chat.*
import java.text.SimpleDateFormat
import com.spotpromo.spotchat.R
import com.spotpromo.spotchat.config.ChatConfig
import com.spotpromo.spotchat.model.*
import com.spotpromo.spotchat.utils.tasks.TaskPush
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class ChatActivity : AppCompatActivity(), CallBack_Projeto {

    private var textViewNome: TextView? = null
    private var circleImageViewFoto: CircleImageView? = null
    //private var searchView: MaterialSearchView? = null
    private var usuarioDestinatario: Contato? = null
    private var edtMensagem: EditText? = null
    private var childEventListener: ChildEventListener? = null
    private var childEventListenerGrupos: ChildEventListener? = null

    private var idUsuarioRemetente: String? = null
    private var idUsuarioDestinatario: String? = null

    private var adapter: MensagemAdapter? = null

    private val mensagens = ArrayList<Mensagem>()
    private val listaGrupoContatos: MutableList<Grupo?> = ArrayList<Grupo?>()
    private val listaContatosGrupo: ArrayList<Contato> = ArrayList<Contato>()
    private var database: DatabaseReference? = null
    private var mensagensRef: DatabaseReference? = null
    private var grupoRef: DatabaseReference? = null
    private var imgChatCamera: ImageView? = null
    private val SELECAO_CAMERA = 100
    private val SELECAO_GALERIA = 200
    private var storageReference: StorageReference? = null
    private var grupo: Grupo? = null
    private val grupoAtualizado: Grupo? = null
    private var mLogin: Login? = null

    // private List<String>mKeys;
    private var nomeGrupo = ""
    private var mAnexo = ""
    private var oldestPostId: String? = null
    private val formattedDate: String? = null
    private var queryGrupo: Query? = null
    private var chaveMsg: String? = ""
    private var contador = 0
    private var fotoEnviada = ""

    private var fbUp: FloatingActionButton? = null
    private var fbDown: FloatingActionButton? = null
    private var ultimoClick: Int? = 0
    private lateinit var reclycler_msg_chat: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat)
        toolbar.title = ""
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        // Loading.show(getApplicationContext(),"sending");
        textViewNome = findViewById(R.id.textViewNomeChat)
        circleImageViewFoto = findViewById(R.id.circleImageFoto)
        edtMensagem = findViewById(R.id.edt_mensagem)
        imgChatCamera = findViewById(R.id.imgChatCamera)
        fbUp = findViewById(R.id.fabUp)
        fbDown = findViewById(R.id.fabDown)

        storageReference = ConfiguracaoFirebase.getFirebaseStorage()
        database = ConfiguracaoFirebase.getFirebaseDatabase()

        fabEnviar.setOnClickListener{
            enviarMensagem()
        }

        material_search_conversa.setOnSearchViewListener(object : SearchViewListener {
            override fun onSearchViewShown() {
                circleImageFoto.visibility = View.GONE
                textViewNomeChat.visibility = View.GONE
                lnLocalizar.visibility = View.VISIBLE
            }

            override fun onSearchViewClosed() {
                circleImageFoto.visibility = View.VISIBLE
                textViewNomeChat.visibility = View.VISIBLE
                lnLocalizar.visibility = View.GONE
            }
        })
        material_search_conversa.setOnQueryTextListener(object : MaterialSearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String): Boolean {
                pesquisarConversas(newText)
                return true
            }
        })
        val bundle = intent.extras
        if (bundle != null) {
            if (bundle.getString("chaveMsg") != null) {
                chaveMsg = bundle.getString("chaveMsg")
            }
            var foto: String? = null
            if (bundle.containsKey("chatGrupo")) {
                grupo = bundle.getSerializable("chatGrupo") as Grupo?
                idUsuarioDestinatario = grupo!!.id
                textViewNomeChat.text = grupo!!.nome
                grupoRef = database!!.child("grupos")
                queryGrupo = grupoRef!!.orderByKey().equalTo(grupo!!.id)
                nomeGrupo = grupo!!.id!!.toString()
                if (grupo!!.foto!!.isNotEmpty()) foto = grupo!!.foto
            } else {
                usuarioDestinatario = bundle.getSerializable("chatContato") as Contato?
                textViewNomeChat.text = usuarioDestinatario!!.getNome()
                //idUsuarioDestinatario = Base64Custom.codificarBase64(usuarioDestinatario.getCodPessoa().toString());
                idUsuarioDestinatario = usuarioDestinatario!!.getCodPessoa().toString()
                if (usuarioDestinatario!!.getFoto()!!.isNotEmpty()) foto = usuarioDestinatario!!.getFoto()
            }
            if (foto != null) {
                val url = Uri.parse(foto)
                Glide.with(this@ChatActivity)
                    .load(url)
                    .into(circleImageFoto)
            } else {
                circleImageFoto.setImageResource(R.drawable.padrao)
            }
            try {
                mLogin = Login().retornar(this)
            } catch (e: Exception) {
                e.printStackTrace()
            }
            idUsuarioRemetente = mLogin!!.codPessoa.toString()
        }

        //configuracao adapter
        adapter = MensagemAdapter(mensagens , this, idUsuarioRemetente!!, this, nomeGrupo, "")
        // adapter.setHasStableIds(true);
        val layoutManagerMsg: RecyclerView.LayoutManager = LinearLayoutManager(this)
        reclycler_msg_chat = findViewById<RecyclerView>(R.id.recycler_chat)
        reclycler_msg_chat.layoutManager = layoutManagerMsg
        reclycler_msg_chat.setHasFixedSize(true)
        reclycler_msg_chat.adapter = adapter

        mensagensRef = database!!.child("mensagens")
            .child(idUsuarioRemetente!!)
            .child(idUsuarioDestinatario!!)

        imgChatCamera!!.setOnClickListener {
            Util.abrirGaleriaChat(
                this@ChatActivity,
                SELECAO_GALERIA
            )
        }
        fbUp!!.setOnClickListener(View.OnClickListener {
            try {
                try {
                    if (ultimoClick == 0 || ultimoClick == null) {
                        ultimoClick = mensagens[mensagens.size - 1]!!.posicao!!
                    } else {
                        var posicao = 0
                        for (i in mensagens.indices) {
                            if (mensagens[i]!!.posicao != null) {
                                if (mensagens[i]!!.posicao != null && mensagens[i]!!.posicao!! > 0) {
                                    if (ultimoClick!! > 0 && ultimoClick == posicao - 1) {
                                        posicao = i
                                        break
                                    } else {
                                        posicao = i - 1
                                    }
                                }
                            }
                        }
                        if (posicao > mensagens.size) {
                            posicao = 0
                        }
                        ultimoClick = mensagens[posicao - 1]!!.posicao
                    }
                    Log.i("ultimoClick", ultimoClick.toString() + "")
                    reclycler_msg_chat.smoothScrollToPosition(ultimoClick!!)
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        })
        fbDown!!.setOnClickListener {
            try {
                if (ultimoClick == 0 || ultimoClick == null) {
                    ultimoClick = mensagens[mensagens.size - 1].posicao
                } else {
                    var posicao = 0
                    for (i in mensagens.indices) {
                        if (mensagens[i].posicao != null ) {
                            if (mensagens[i]!!.posicao!! > 0) {
                                if (mensagens[i].posicao === ultimoClick) {
                                    posicao = i + 1
                                }
                            }
                        }
                    }
                    if (posicao < 0) {
                        posicao = 0
                    }
                    ultimoClick = mensagens[posicao].posicao
                }
                Log.i("ultimoClick", ultimoClick.toString() + "")
                reclycler_msg_chat.smoothScrollToPosition(ultimoClick!!)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    fun listaPosicaoScroll(): ArrayList<Int>? {
        val listaPosicao = ArrayList<Int>()
        for (i in mensagens.indices) {
            Log.i("posicao", mensagens[i].isSearch.toString() + " " + i + "")
            if (mensagens[i]!!.posicao!! > 0) {
                listaPosicao.add(mensagens[i]!!.posicao!!)
                reclycler_msg_chat.getLayoutManager()!!.scrollToPosition(i);
            }
        }
        return listaPosicao
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        try {
            if (resultCode == RESULT_OK && requestCode == SELECAO_GALERIA) {
                data?.data?.also { uri ->
                    mAnexo = Util.retornaCaminhoFotoURINew(uri, 0, "Chat_Galeria", this)!!

                }
            }
        } catch (e: Exception) {
            Alerta.show(this@ChatActivity,"Atenção","Erro ao tentar anexar",false)
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onStart() {
        super.onStart()
        recuperarMensagem()
        if (nomeGrupo.isNotEmpty()) retornaListaContatosAtualizada()
    }

    override fun onStop() {
        super.onStop()
//        mensagensRef!!.removeEventListener(childEventListener!!)
    }

    override fun onRetornoFotoGrupo(aBolean: Boolean, data: String?, mensagem: String?) {
        TODO("Not yet implemented")
    }

    override fun onRetorno(mensagem: Mensagem) {
        TODO("Not yet implemented")
    }

    override fun onRetorno(aBolean: Boolean, data: String?, mensagem: String?) {
        TODO("Not yet implemented")
    }


    override fun onRetorno(aBolean: Boolean, data: String?, mensagem: String?, nomeArquivo: String?) {
        try {
            if (aBolean) {
                if (mAnexo.isNotEmpty()) {
                    val dataFoto: String = FormataData.retornaDataFormat(FormataData.Numerico)
                    fotoEnviada = ChatConfig.url_chat_photo
                        .replace("_DATA", dataFoto).replace(
                            "_ARQUIVO",
                            nomeArquivo!!
                        )
                }
                if (usuarioDestinatario != null) {
                    val msg: Mensagem = criarMensagem(
                        idUsuarioRemetente,
                        mensagem,
                        fotoEnviada,
                        mLogin!!.nomPessoa,
                        null
                    )

                    //Salvar Mensagem do Remetente
                    msg.salvar(idUsuarioRemetente, idUsuarioDestinatario, msg)

                    //Salvar Mensagem do Destinatario
                    msg.salvar(idUsuarioDestinatario, idUsuarioRemetente, msg)
                    edtMensagem!!.setText("")
                    mAnexo = ""
                    fotoEnviada = ""
                } else {
                    val grupoMensagem: GrupoMensagem =
                        criaGrupoMensagem(mLogin!!.codPessoa, mensagem, fotoEnviada)
                    val listaNotificacao: ArrayList<Push> = ArrayList<Push>()
                    for (membro in grupo!!.usuarios!!) {
                        val idRemetenteGrupo: String = membro.getCodPessoa().toString()
                        val idUsuarioLogadoGrupo: String = mLogin!!.codPessoa.toString()
                        val msg: Mensagem = criarMensagem(
                            idUsuarioLogadoGrupo,
                            mensagem,
                            fotoEnviada,
                            membro.getNome(),
                            grupoMensagem.getIdGrupoMensagem()
                        )


                        //salvar membro
                        msg.salvar(idUsuarioDestinatario, idRemetenteGrupo, msg)
                        salvarConversa(
                            idRemetenteGrupo,
                            idUsuarioDestinatario,
                            usuarioDestinatario,
                            msg,
                            true
                        )
                        if (mLogin!!.codPessoa.toString() != membro.getCodPessoa().toString()
                        ) {
                            val push = Push()
                            push.method = "enviarPush"
                            push.codUserOrigem  = mLogin!!.codPessoa.toString()
                            //push.setCodUserDestino(idUsuarioDestinatario);
                            push.msg = "Send file"
                            TaskPush(this).execute(push)
                            //listaNotificacao.add(push);
                        }
                    }
                    edtMensagem!!.setText("")
                    mAnexo = ""
                    fotoEnviada = ""

                    //String jsonDados = new Gson().toJson(listaNotificacao);
                    //new TaskPush(getBaseContext()).execute(push);
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onRetorno(aBoolean: Boolean, mensagem: String) {
        TODO("Not yet implemented")
    }

    fun enviarMensagem() {
        val textoMensagem = edtMensagem!!.text.toString()
        if (mAnexo.isNotEmpty()) {
            Alerta.show(
                this, "Envio de Dados?", "Por favor confirme o envio dos dados?", "Yes",
                DialogInterface.OnClickListener { dialog, which ->
                    TaskFotoChat(this@ChatActivity).execute(
                        mAnexo,
                        textoMensagem
                    )
                }, false
            )
        } else {
            if (mAnexo.isNotEmpty()) {
                val data: String = FormataData.retornaDataFormat(FormataData.Numerico)
                fotoEnviada =
                    ChatConfig.url_chat_photo.replace("_DATA", data)
                        .replace("_ARQUIVO", Util.retonarNomeFoto(mAnexo))
            }
            if (!textoMensagem.isEmpty()) {
                val asyncTask = if (usuarioDestinatario != null) {
                    val mensagem: Mensagem = criarMensagem(
                        idUsuarioRemetente,
                        textoMensagem,
                        fotoEnviada,
                        mLogin!!.nomPessoa,
                        null
                    )

                    //Salvar Mensagem do Remetente
                    mensagem.salvar(idUsuarioDestinatario, idUsuarioRemetente, mensagem)
                    //Salvar Mensagem do Destinatario
                    mensagem.salvar(idUsuarioRemetente, idUsuarioDestinatario, mensagem)
                    edtMensagem!!.setText("")
                    var foto = ""
                    if (mLogin!!.fotoPerfil !== "") {
                        foto = ChatConfig.url_profile_photo
                            .replace(
                                "_CODPESSOA",
                                java.lang.String.valueOf(mLogin!!.codPessoa).toString()
                            )
                            .replace("_IMAGEM", mLogin!!.fotoPerfil.toString())
                    }

                    //Salvar Mensagem do Remetente
                    val usuarioEnviandoMsg = Contato()
                    usuarioEnviandoMsg.setCodPessoa(mLogin!!.codPessoa)
                    usuarioEnviandoMsg.setNome(mLogin!!.nomPessoa)
                    usuarioEnviandoMsg.setFoto(foto)
                    //Usuario usuarioEnviandoMsg = UsuarioFirebase.getDadosUsuarioLogado();
                    salvarConversa(
                        idUsuarioDestinatario,
                        idUsuarioRemetente,
                        usuarioEnviandoMsg,
                        mensagem,
                        false
                    )

                    //Salvar Mensagem do Destinatario
                    salvarConversa(
                        idUsuarioRemetente,
                        idUsuarioDestinatario,
                        usuarioDestinatario,
                        mensagem,
                        false
                    )
                    val listaUsuarios = ArrayList<String>()
                    listaUsuarios.add(idUsuarioDestinatario!!)
                    val push = Push()
                    push.method = "enviarPush"
                    push.codUserOrigem = mLogin!!.codPessoa.toString()
                    push.codUserDestino = listaUsuarios
                    //push.setCodUserDestino();
                    push.msg = textoMensagem

                    //listaNotificacao.add(push);

                    //String jsonDados = new Gson().toJson(push);
                    TaskPush(baseContext).execute(push)
                } else {

                    //TODO: Ver aonde está usando
                    val grupoMensagem: GrupoMensagem =
                        criaGrupoMensagem(mLogin!!.codPessoa, textoMensagem, fotoEnviada)
                    var listaNotificacao = ArrayList<String>()
                    val idUsuarioLogadoGrupo: String = mLogin!!.codPessoa.toString()
                    val mensagem = criarMensagem(
                        idUsuarioLogadoGrupo,
                        textoMensagem,
                        fotoEnviada,
                        mLogin!!.nomPessoa,
                        grupoMensagem.getIdGrupoMensagem()
                    )
                    for (membro in grupo!!.usuarios!!) {
                        val idRemetenteGrupo: String = membro.getCodPessoa().toString()

                        //salvar membro
                        mensagem.salvar(idUsuarioDestinatario, idRemetenteGrupo, mensagem)
                        edtMensagem!!.setText("")
                        salvarConversa(
                            idRemetenteGrupo,
                            idUsuarioDestinatario,
                            usuarioDestinatario,
                            mensagem,
                            true
                        )
                        if (mLogin!!.codPessoa.toString() != membro.getCodPessoa().toString()
                        ) {
                            listaNotificacao.add(membro.getCodPessoa().toString())
                        }
                    }
                    val push = Push()
                    push.method = "enviarPush"
                    push.codUserOrigem = mLogin!!.codPessoa.toString()
                    //push.setCodUserDestino(usuarios);
                    push.msg = textoMensagem
                    push.codUserDestino= listaNotificacao

                    //String jsonDados = new Gson().toJson(push);
                    TaskPush(baseContext).execute(push)
                }
                mAnexo = ""
                fotoEnviada = ""

                //}
            } else {
                Toast.makeText(this, "Preencha uma mensagem", Toast.LENGTH_LONG).show()
            }
        }
    }


    fun criarMensagem(
        idUsuario: String?,
        texto: String?,
        foto: String,
        nome: String?,
        idGrupoMensagem: String?
    ): Mensagem {
        val mensagem = Mensagem()
        mensagem.idUsuario = idUsuario
        mensagem.mensagem = texto
        mensagem.imagem = foto
        mensagem.lido = 0
        mensagem.data =gmtTime()
        mensagem.nome = nome
        mensagem.flImagem = foto.isNotEmpty()
        mensagem.idGrupoMensagem = idGrupoMensagem
        mensagem.flExcluida =0
        mensagem.msgAdmin = ""
        return mensagem
    }


    fun criaGrupoMensagem(idUsuario: Int?, texto: String?, foto: String): GrupoMensagem {
        val grupoMensagem = GrupoMensagem()
        grupoMensagem.setIdRemetente(idUsuario)
        grupoMensagem.setMensagem(texto)
        grupoMensagem.setData(gmtTime())
        grupoMensagem.setImagem(foto)
        grupoMensagem.setFlImagem(foto.isNotEmpty())
        return grupoMensagem
    }

    fun gmtTime(): String? {
        val sf = SimpleDateFormat("yyyy-MM-dd kk:mm", Locale.ENGLISH)
        sf.timeZone = TimeZone.getTimeZone("UTC")
        return sf.format(Date())
    }

    private fun atualizaLeitura(idRementente: String?, idDestinatario: String?, msg: Mensagem?) {
        val caminho = "$idDestinatario/$idRementente/"
        val database: DatabaseReference = ConfiguracaoFirebase.getFirebaseDatabase()!!
        val mensagemRef = database.child("mensagens")
        /* mensagemRef.child(caminho).child(msg.getId())
                .setValue(msg);

        mensagemRef.child(caminho).updateChildren(msg);*/
        val hashMap: MutableMap<String, Any?> = HashMap()
        hashMap[caminho + "/" + msg!!.id + "/"] = msg
        mensagemRef.updateChildren(hashMap)
    }


    private fun atualizaLeituraGrupo(
        idGrupo: String?,
        idMensagem: String,
        grupoMensagem: GrupoMensagem
    ) {
        val listaContatos: ArrayList<Contato> = ArrayList<Contato>()
        val caminho = "$idGrupo/$idMensagem"
        val database: DatabaseReference = ConfiguracaoFirebase.getFirebaseDatabase()!!
        for (membro in grupo!!.usuarios!!) {
            if (membro.getCodPessoa().toString() == mLogin!!.codPessoa.toString()) {
                membro.setLido(1)
            } else {
                membro.setLido(0)
            }
            val valida: Boolean =
                membro.getCodPessoa().toString() == mLogin!!.codPessoa.toString()
            Log.i(
                "caminhoGrupo",
                caminho + "-" + mLogin!!.codPessoa + membro.getCodPessoa() + "-" + membro.getLido() + "-" + valida
            )
            listaContatos.add(membro)
        }
        grupoMensagem.setListaContatos(listaContatos)
        val mensagemRef = database.child("grupoMensagem")
        mensagemRef.child(idGrupo!!)
            .child(grupoMensagem.getIdGrupoMensagem()!!)
            .setValue(grupoMensagem)
    }


    private fun salvarConversa(
        idRemetente: String?,
        idDestinatario: String?,
        usuarioExibicao: Contato?,
        msg: Mensagem,
        isGroup: Boolean
    ) {
        val conversaRemetente = Conversa()
        conversaRemetente.idDestinatario = idDestinatario
        conversaRemetente.idRemetente = idRemetente
        conversaRemetente.ultimaMensagem =  msg.mensagem
        conversaRemetente.lido = 0
        if (!isGroup) {
            conversaRemetente.usuarioExibicao= usuarioExibicao
            conversaRemetente.isGroup = "false"
        } else {
            conversaRemetente.isGroup = "true"
            conversaRemetente.grupo = grupo
        }
        conversaRemetente.salvar()
    }

    private fun atualizarConversa(
        idRemetente: String?,
        idDestinatario: String?,
        usuarioExibicao: Contato?,
        msg: Mensagem?,
        isGroup: Boolean
    ) {
        val conversaRemetente = Conversa()
        conversaRemetente.idDestinatario = idDestinatario
        conversaRemetente.idRemetente = idRemetente
        conversaRemetente.ultimaMensagem =  msg!!.mensagem
        conversaRemetente.lido = 1
        if (!isGroup) {
            conversaRemetente.usuarioExibicao = usuarioExibicao
            conversaRemetente.isGroup = "false"
        } else {
            conversaRemetente.isGroup = "true"
            conversaRemetente.grupo = grupo
        }
        conversaRemetente.salvar()
    }


    private fun recuperarMensagem() {
        mensagens.clear()
        contador = 0
        val childEventListener = object : ChildEventListener {
            override fun onChildAdded(dataSnapshot: DataSnapshot, previousChildName: String?) {

                    try {
                        val mensagem = dataSnapshot.getValue<Mensagem>()
                        Log.i("mensagem", mensagem!!.id!!)
                        contador++
                        oldestPostId = dataSnapshot.key
                        mensagens.add(mensagem)
                        Log.i("tamanho mensagem", mensagens.size.toString())
                        if (nomeGrupo.length == 0) {
                            atualizarConversa(
                                idUsuarioRemetente,
                                idUsuarioDestinatario,
                                usuarioDestinatario,
                                mensagem,
                                false
                            )
                            if (mensagem.lido == 0) {
                                if (mLogin!!.codPessoa.toString() != mensagem.idUsuario
                                ) {
                                    val m2: Mensagem? = mensagem
                                    m2!!.lido == 1
                                    atualizaLeitura(idUsuarioDestinatario, idUsuarioRemetente, m2)
                                }
                                if (mLogin!!.codPessoa.toString() != mensagem.idUsuario
                                ) {
                                    val m2: Mensagem? = mensagem
                                    m2!!.lido = 1
                                    atualizaLeitura(idUsuarioRemetente, idUsuarioDestinatario, m2)
                                }
                            }
                        } else {
                            val grupoMensagem = GrupoMensagem()
                            grupoMensagem.setIdGrupoMensagem(mensagem.idGrupoMensagem)
                            grupoMensagem.setMensagem(mensagem.mensagem)
                            grupoMensagem.setData(mensagem.data)
                            grupoMensagem.setIdRemetente(mensagem.idUsuario!!.toInt())
                            grupoMensagem.setImagem(mensagem.imagem)
                            atualizarConversa(
                                idUsuarioRemetente,
                                idUsuarioDestinatario,
                                usuarioDestinatario,
                                mensagem,
                                true
                            )
                            atualizaLeituraGrupo(
                                idUsuarioDestinatario,
                                mensagem.idGrupoMensagem!!,
                                grupoMensagem
                            )
                            adapter!!.notifyDataSetChanged()
                        }
                        if (chaveMsg == mensagem.id) {
                            reclycler_msg_chat.smoothScrollToPosition(contador)
                        }
                        if (chaveMsg!!.isEmpty()) {
                            reclycler_msg_chat.smoothScrollToPosition(mensagens.size - 1)
                        }
                        adapter!!.notifyDataSetChanged()
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

            }

            override fun onChildChanged(dataSnapshot: DataSnapshot, previousChildName: String?) {
                if (dataSnapshot != null) {
                    try {
                        val mMensagem =  dataSnapshot.getValue<Mensagem>()
                        val chave = dataSnapshot.key
                        for (m1 in mensagens) {
                            Log.i("id-fora", chave + "-++" + m1!!.id)
                            if (m1.id.equals(chave)) {
                                Log.i("id-dentro", m1.id!!)
                                mensagens.remove(m1)
                                mensagens.add(mMensagem!!)
                                adapter!!.notifyDataSetChanged()
                            }
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }

            override fun onChildRemoved(snapshot: DataSnapshot) {
            }

            override fun onChildMoved(snapshot: DataSnapshot, previousChildName: String?) {
            }

            override fun onCancelled(error: DatabaseError) {
            }
        }
        mensagensRef!!.addChildEventListener(childEventListener)

    }

    @Throws(Exception::class)
    operator fun contains(valor: String): Boolean {
        var retorno = false
        //".docx", ".doc", ".xlsx", ".xls",
        val listaArquivo = arrayOf(".jpeg", ".jpg", ".png", ".pdf", ".mp4")
        for (i in listaArquivo.indices) {
            Log.i("valor", valor)
            if (valor.contains(listaArquivo[i])) {
                retorno = true
                break
            }
        }
        return retorno
    }


    fun retornaListaContatosAtualizada() {
        listaGrupoContatos.clear()
        listaContatosGrupo.clear()
        childEventListenerGrupos = queryGrupo!!.addChildEventListener(object : ChildEventListener {
            override fun onChildAdded(dataSnapshot: DataSnapshot, s: String?) {
                try {
                    val gruposDados: Grupo? = dataSnapshot.getValue(Grupo::class.java)
                    listaGrupoContatos.add(gruposDados)
                    listaContatosGrupo.addAll(gruposDados!!.usuarios!!)
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onChildChanged(dataSnapshot: DataSnapshot, s: String?) {
                try {
                    val gruposDados: Grupo? = dataSnapshot.getValue(Grupo::class.java)
                    listaGrupoContatos.add(gruposDados)
                    listaContatosGrupo.clear()
                    listaContatosGrupo.addAll(gruposDados!!.usuarios!!)
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onChildRemoved(dataSnapshot: DataSnapshot) {
                try {
                    val gruposDados: Grupo? = dataSnapshot.getValue(Grupo::class.java)
                    listaGrupoContatos.add(gruposDados)
                    listaContatosGrupo.clear()
                    listaContatosGrupo.addAll(gruposDados!!.usuarios!!)
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onChildMoved(dataSnapshot: DataSnapshot, s: String?) {}
            override fun onCancelled(databaseError: DatabaseError) {}
        })
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_chat, menu)
        val item = menu.findItem(R.id.menuPesquisa)
        material_search_conversa!!.setMenuItem(item)
        return super.onCreateOptionsMenu(menu)
    }


    fun pesquisarConversas(texto: String) {
        if (mensagens.size > 0 && texto.isNotEmpty()) {
            for (i in mensagens.indices) {
                if (mensagens[i].mensagem != null) {
                    if (mensagens[i].mensagem!!.isNotEmpty()) {
                        val textoMensagem: String = mensagens[i].mensagem!!.toLowerCase()
                        if (textoMensagem.contains(texto)) {
                            mensagens[i].posicao = i
                        } else {
                            mensagens[i].posicao = 0
                        }
                    }
                }
            }
            adapter = MensagemAdapter(mensagens!!, this, idUsuarioRemetente!!, this, nomeGrupo, texto)
            reclycler_msg_chat.adapter = adapter
            adapter!!.notifyDataSetChanged()
        }
    }

}