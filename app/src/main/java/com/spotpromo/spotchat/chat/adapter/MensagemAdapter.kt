package com.spotpromo.spotchat.chat.adapter

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.graphics.drawable.BitmapDrawable
import android.net.Uri
import android.os.Environment
import android.text.util.Linkify
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.VideoView
import androidx.recyclerview.widget.RecyclerView
import com.spotpromo.spotchat.chat.activity.ChatActivity
import com.spotpromo.spotchat.chat.activity.MensagemActivity
import com.bumptech.glide.Glide
import com.spotpromo.spotchat.R
import com.spotpromo.spotchat.model.Mensagem
import com.spotpromo.spotchat.utils.*
import ir.siaray.downloadmanagerplus.classes.Downloader
import java.io.File

class MensagemAdapter(
    val lista: ArrayList<Mensagem>?,
    val context: Context?,
    val idUsuario: String,
    val chatActivity: ChatActivity?,
    val  grupo: String?,
    val texto: String?
) :  RecyclerView.Adapter<MensagemAdapter.MyViewHolder>() {

    private val TIPO_REMETENTE = 0
    private val TIPO_DESTINATARIO = 1
    private var textoProcurado: String? = null


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        var item: View? = null
        if (viewType == TIPO_REMETENTE) {
            item = LayoutInflater.from(parent.context)
                .inflate(R.layout.adapter_mensagem_remetente, parent, false)
        } else if (viewType == TIPO_DESTINATARIO) {
            item = LayoutInflater.from(parent.context)
                .inflate(R.layout.adapter_mensagem_destinatario, parent, false)
        }
        return MyViewHolder(item)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val mensagem = lista!![position]
        val msg: String? = mensagem.mensagem
        val imagem: String? = mensagem.imagem
        val nome: String? = mensagem.nome
        var data = ""
        try {
            if (mensagem.data != null) {
                data =
                    Util.uTCToLocal("yyyy-MM-dd kk:mm", "MM/dd/yyyy hh:mm a", mensagem.data)
                        .toString()
                holder.txtData.text = data
            }
            if (!nome!!.isEmpty()) {
                holder.txtNome.text = nome
                holder.txtNome.setOnLongClickListener {
                    val intent = Intent(chatActivity, MensagemActivity::class.java)
                    intent.putExtra("mensagem", mensagem)
                    intent.putExtra("nomeGrupo", grupo)
                    chatActivity!!.startActivity(intent)
                    true
                }
            } else {
                holder.txtNome.visibility = View.GONE
            }
            holder.txtMensagem.visibility = View.VISIBLE
            if (msg != null) {
                if (mensagem.flExcluida == 0) {
                    holder.txtMensagem.text = msg
                    Linkify.addLinks(holder.txtMensagem, Linkify.WEB_URLS)
                    if (msg.isNotEmpty()) Util.setTags(holder.txtMensagem, msg)
                } else {
                    holder.txtMensagem.text = "This message has been deleted."
                }
            }
            Util.setSearchTextHighlightSimpleSpannable(holder.txtMensagem, msg, textoProcurado)


            //validação para mostrar o check de lido apenas para conversa 1 a 1
            holder.imageCheck.visibility = View.GONE
            if (nome.isEmpty()) {
                if (idUsuario == mensagem.idUsuario) {
                    if (mensagem.lido === 1) {
                        holder.imageCheck.visibility = View.VISIBLE
                    }
                }
            }
            if (mensagem.flImagem) {
                if (mensagem.flExcluida == 0) {
                    Log.i("tipoFoto",mensagem!!.imagem.toString().substring(mensagem!!.imagem.toString().lastIndexOf(".")))
                    if (mensagem!!.imagem.toString()
                            .substring(mensagem!!.imagem.toString().lastIndexOf("."))
                            .contains("pdf")
                    ) {
                        /*holder.imageMensagem.visibility = View.VISIBLE
                        holder.imageMensagem.setImageResource(R.drawable.ic_pdf)
                        holder.imageMensagem.setOnClickListener {
                            val intent =
                                Intent(Intent.ACTION_VIEW, Uri.parse(mensagem.imagem))
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                            intent.setPackage("com.android.chrome")
                            try {
                                context!!.startActivity(intent)
                            } catch (ex: ActivityNotFoundException) {
                                // Chrome browser presumably not installed so allow user to choose instead
                                intent.setPackage(null)
                                context!!.startActivity(intent)
                            }
                        }*/
                    }
                else if (mensagem!!.imagem.toString()
                        .substring(mensagem.imagem!!.lastIndexOf("."))
                        .contains("mp4")
                ) {
                    holder.imageMensagem.visibility = View.VISIBLE
                    val caminho = Environment.getExternalStorageDirectory()
                        .toString() + "/SGDM_LG_FABBER/SPOT_FOTOS/chat/"
                    val nomeArquivo = FileUtilidades.getFileNameFromURL(mensagem.imagem).toString()
                    val file = File(caminho + File.separator + nomeArquivo)
                    Log.i("caminho", caminho + File.separator + nomeArquivo)
                    Log.i("fotoCaminho", nomeArquivo + "//" + file.isFile)
                    if (!file.isFile) {
                        holder.imageMensagem.setImageResource(R.drawable.ic_video)
                    } else {
                        Glide.with(context!!)
                            .asBitmap()
                            .load(file.absolutePath)
                            .into(holder.imageMensagem)
                    }
                    holder.imageMensagem.setOnClickListener { v ->
                        if (!file.isFile) {
                            Alerta.show(
                                v.context,
                                v.context.resources.getString(R.string.biblioteca_download_arquivo_pdf_alerta),
                                context!!.resources.getString(R.string.mensagem_download_chat),
                                context!!.resources.getString(R.string.btn_sim),
                                DialogInterface.OnClickListener { dialog, which ->
                                    try {
                                        val downloader = Downloader.getInstance(context)
                                            .setUrl(mensagem.imagem)
                                            .setVisibleInDownloadsUi(true)
                                            .setDestinationDir(caminho, nomeArquivo)
                                            .setNotificationTitle("Video Download")
                                            .setDescription("Confirm download this file?")
                                            .setKeptAllDownload(true)
                                        downloader.start()
                                    } catch (err: Exception) {
                                        LogTrace.logCatch(
                                            context,
                                            context!!.javaClass,
                                            err,
                                            true
                                        )
                                    }
                                },
                                false
                            )
                        }
                        else {
                            try {
                                val videoFile = File(file.absolutePath)
                                val fileUri = Uri.parse(videoFile.absolutePath)
                                val intent = Intent(Intent.ACTION_VIEW)
                                intent.setDataAndType(fileUri, "video/*")
                                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION) //DO NOT FORGET THIS EVER
                                context!!.startActivity(intent)
                            } catch (e: Exception) {
                                e.printStackTrace()
                            }
                        }
                    }
                } else {
                    holder.imageMensagem.visibility = View.VISIBLE
                    CarregaImagemDaUrl.buscar(context!!, imagem!!, holder.imageMensagem)
                    holder.imageMensagem.setOnClickListener { v ->
                        val mBitmap = (holder.imageMensagem.drawable as BitmapDrawable).bitmap
                        if (mBitmap != null) {
                            try {
                                if (chatActivity != null) {
                                    Util.showImageGalleryChat(context, mBitmap, v, chatActivity)
                                }
                            } catch (e: Exception) {
                                e.printStackTrace()
                            }
                        }
                    }
                }
                }
                else {
                    holder.txtMensagem.text = "This message has been deleted."
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun getItemCount(): Int {
        return if (lista == null) {
            0
        } else {
            lista!!.size
        }
    }

    override fun getItemViewType(position: Int): Int {
        val mensagem= lista!![position]
        return if (idUsuario == mensagem.idUsuario) {
            TIPO_REMETENTE
        } else TIPO_DESTINATARIO
    }

    class MyViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView!!)
    {
        var txtMensagem: TextView
        var txtNome: TextView
        var txtData: TextView
        var imageMensagem: ImageView
        var imageCheck: ImageView
        var video: VideoView
        var linha: LinearLayout

        init {
            txtNome = itemView!!.findViewById(R.id.textNomeExibicao)
            txtMensagem = itemView.findViewById(R.id.textMensagemTexto)
            imageMensagem = itemView.findViewById(R.id.imageMensagemFoto)
            imageCheck = itemView.findViewById(R.id.imgCheck)
            video = itemView.findViewById(R.id.videoChat)
            txtData = itemView.findViewById(R.id.txtData)
            linha = itemView.findViewById(R.id.ln_linha)
        }
    }


}