package com.spotpromo.spotchat.chat.activity

import android.content.Intent
import android.graphics.Bitmap
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.widget.EditText
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.spotpromo.spotchat.chat.adapter.GrupoSelecionadoAdapter
import com.spotpromo.spotchat.model.Grupo
import com.spotpromo.spotchat.utils.tasks.TaskFotoGrupo
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.firebase.storage.StorageReference
import com.spotpromo.spotchat.R
import com.spotpromo.spotchat.config.ChatConfig
import com.spotpromo.spotchat.controller.CallBack_Projeto
import com.spotpromo.spotchat.model.Contato
import com.spotpromo.spotchat.model.Login
import com.spotpromo.spotchat.model.Mensagem
import com.spotpromo.spotchat.utils.Util
import com.spotpromo.spotchat.utils.firebase.ConfiguracaoFirebase
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.content_cadastro_grupo.*
import java.io.File
import java.util.*
import kotlin.collections.ArrayList

class CadastroGrupoActivity : AppCompatActivity(), CallBack_Projeto {


    val listaMembrosSelecionado = ArrayList<Contato>()
    var grupoSelecionadoAdapter: GrupoSelecionadoAdapter? = null
    var reclyclerMembrosSelecionados: RecyclerView? = null
    var edtNomeGrupo: EditText? = null
    var imgNomeGrupo: CircleImageView? = null
    val SELECAO_GALERIA = 1
    var storageReference: StorageReference? = null
    var grupo: Grupo? = null
    var fabSalvarGrupo: FloatingActionButton? = null
    var mLogin: Login? = null
    var foto: String? = null
    var caminhoFoto = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cadastro_grupo)
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        toolbar.title = "New Group"
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        reclyclerMembrosSelecionados = findViewById(R.id.recyclerMembrosCadGrupos)
        edtNomeGrupo = findViewById(R.id.edtNomeGrupo)
        imgNomeGrupo = findViewById(R.id.imgFotoGrupo)
        fabSalvarGrupo = findViewById(R.id.fabSalvarGrupo)
        grupo = Grupo()
        try {
            mLogin = Login().retornar(getBaseContext())
        } catch (e: Exception) {
            e.printStackTrace()
        }
        storageReference = ConfiguracaoFirebase.getFirebaseStorage()
        imgNomeGrupo!!.setOnClickListener {
            val i = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
            if (i.resolveActivity(getPackageManager()) != null) {
                startActivityForResult(i, SELECAO_GALERIA)
            }
        }
        fabSalvarGrupo!!.setOnClickListener {
            TaskFotoGrupo(this@CadastroGrupoActivity).execute(
                caminhoFoto
            )
        }
        if (intent.extras != null) {
            listaMembrosSelecionado.addAll(
                intent.extras!!.getSerializable("membros") as ArrayList<Contato>
            )
        }
        txtParticipantes.text = getString(R.string.participantes)
            .replace("%s", listaMembrosSelecionado.size.toString())
        grupoSelecionadoAdapter =
            GrupoSelecionadoAdapter(listaMembrosSelecionado, applicationContext)
        val layoutManagerMembros: RecyclerView.LayoutManager = LinearLayoutManager(
            applicationContext,
            LinearLayoutManager.HORIZONTAL,
            false
        )
        reclyclerMembrosSelecionados!!.layoutManager = layoutManagerMembros
        reclyclerMembrosSelecionados!!.setHasFixedSize(true)
        reclyclerMembrosSelecionados!!.adapter = grupoSelecionadoAdapter
    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
   override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK) {
            try {
                var imagem: Bitmap? = null
                foto = data!!.data?.let {
                    Util.retornaPathImage(it, this@CadastroGrupoActivity)
                }
                imagem = MediaStore.Images.Media.getBitmap(getContentResolver(), data!!.data)
                val file = File(foto)
                if (file.isFile) {
                    caminhoFoto = file.absolutePath
                    imgNomeGrupo!!.setImageBitmap(imagem)
                }
                /*


                Uri localImagemSelecionada = data.getData();
                imagem = MediaStore.Images.Media.getBitmap(getContentResolver(),localImagemSelecionada);

                if (imagem != null){
                    imgNomeGrupo.setImageBitmap(imagem);

                    ByteArrayOutputStream bao = new ByteArrayOutputStream();
                    imagem.compress(Bitmap.CompressFormat.JPEG,70,bao);
                    byte[] dadosImagem = bao.toByteArray();

                    //Salvar Imagem firebase
                    StorageReference imagemRef = storageReference.child("imagens").child("grupos")
                            .child(grupo.getId()+ ".jpeg");

                    UploadTask uploadTask = imagemRef.putBytes(dadosImagem);
                    uploadTask.addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(CadastroGrupoActivity.this,
                                    "Erro ao Enviar Foto",
                                    Toast.LENGTH_LONG).show();
                        }
                    }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            Task<Uri> urlTask = taskSnapshot.getStorage().getDownloadUrl();
                            while (!urlTask.isSuccessful());
                            Uri downloadUrl = urlTask.getResult();
                            grupo.setFoto(downloadUrl.toString());
                        }
                    });

                }

           */
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }


     override fun onRetornoFotoGrupo(aBolean: Boolean, data: String?, mensagem: String?) {
        try {
            if (aBolean) {
                val nomeGrupo = edtNomeGrupo!!.text.toString()
                //para adicionar o usuario logad
                val contatoLogado = Contato()
                contatoLogado.setCodPessoa(mLogin!!.codPessoa)
                contatoLogado.setNome(mLogin!!.nomPessoa)
                contatoLogado.setRota(mLogin!!.desRota)
                listaMembrosSelecionado.add(contatoLogado)
                val file = File(caminhoFoto)
                grupo!!.usuarios = listaMembrosSelecionado
                grupo!!.nome = nomeGrupo
                grupo!!.codPessoa = mLogin!!.codPessoa
                grupo!!.bu =mLogin!!.codSubBU
                if (file.isFile) {
                    grupo!!.foto =
                        ChatConfig.url_chat_photo
                            .replace("_DATA", data.toString())
                            .replace("_ARQUIVO", Util.retonarNomeFoto(file.name))
                } else {
                    grupo!!.foto = ""
                }
                grupo!!.salvar()
                val i = Intent(this@CadastroGrupoActivity, ChatActivity::class.java)
                i.putExtra("chatGrupo", grupo)
                startActivity(i)
            }
        } catch (e: Exception) {
        }
    }

    override fun onRetorno(mensagem: Mensagem) {
        TODO("Not yet implemented")
    }

    override fun onRetorno(aBolean: Boolean, data: String?, mensagem: String?) {
        TODO("Not yet implemented")
    }

    override fun onRetorno(
        aBolean: Boolean,
        data: String?,
        mensagem: String?,
        nomeArquivo: String?
    ) {
        TODO("Not yet implemented")
    }

    override fun onRetorno(aBoolean: Boolean, mensagem: String) {
        TODO("Not yet implemented")
    }
}