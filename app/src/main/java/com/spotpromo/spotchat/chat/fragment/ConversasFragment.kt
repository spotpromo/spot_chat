package com.spotpromo.spotchat.chat.fragment

import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.spotpromo.spotchat.chat.activity.ChatActivity
import com.spotpromo.spotchat.chat.adapter.ConversasAdapter
import com.spotpromo.spotchat.chat.adapter.ConversasMensagemAdapter
import com.google.firebase.FirebaseError
import com.google.firebase.database.*
import com.google.firebase.database.ktx.getValue
import com.spotpromo.spotchat.model.Conversa
import com.spotpromo.spotchat.model.Login
import com.spotpromo.spotchat.model.Mensagem
import com.spotpromo.spotchat.utils.Alerta
import com.spotpromo.spotchat.utils.RecyclerItemClickListener
import com.spotpromo.spotchat.utils.firebase.ConfiguracaoFirebase
import java.util.*
import com.spotpromo.spotchat.R
import kotlinx.android.synthetic.main.fragment_conversas.*

class ConversasFragment : DialogFragment() {
    private val listaConversas: ArrayList<Conversa> = ArrayList<Conversa>()
    private val listaMensagens: ArrayList<Mensagem> = ArrayList<Mensagem>()
    private var conversasAdapter: ConversasAdapter? = null
    private var conversasMensagemAdapter: ConversasMensagemAdapter? = null
    private var database: DatabaseReference? = null
    private var conversasRef: DatabaseReference? = null
    private var conversasRefBusca: DatabaseReference? = null
    private var mensagemRef: DatabaseReference? = null
    private var childEventListenerConversar: ChildEventListener? = null
    private var mLogin: Login? = null
    private var qConversa: Query? = null
    private var txtMensagem: TextView? = null
    private var txtChat: TextView? = null


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view: View = inflater.inflate(R.layout.fragment_conversas, container, false)
        txtMensagem = view.findViewById(R.id.txtConversaMensagens)
        txtChat = view.findViewById(R.id.txtConversaChats)
        database = ConfiguracaoFirebase.getFirebaseDatabase()

        //configurar Adapter
        conversasAdapter = ConversasAdapter(listaConversas!!, activity!!)
        conversasMensagemAdapter = ConversasMensagemAdapter(listaMensagens, activity!!)

        //configurar recylerView
        val layoutManager = LinearLayoutManager(activity)
        val recyclerListaConversas = view.findViewById<RecyclerView>(R.id.recyclerListaConversas)
        recyclerListaConversas.layoutManager = layoutManager
        recyclerListaConversas.setHasFixedSize(true)
        recyclerListaConversas.adapter = conversasAdapter

        val layoutManagerMsg: RecyclerView.LayoutManager = LinearLayoutManager(activity)
        val recyclerListaMsg = view.findViewById<RecyclerView>(R.id.recyclerListaMsg)
        recyclerListaMsg.layoutManager = layoutManagerMsg
        recyclerListaMsg.setHasFixedSize(true)
        recyclerListaMsg.adapter = conversasMensagemAdapter

        try {
            mLogin = Login().retornar(getActivity()!!)
        } catch (e: Exception) {
            e.printStackTrace()
        }


        val identificadorUsuario: String = mLogin!!.codPessoa.toString()

        conversasRefBusca = database!!.child("conversas").child(identificadorUsuario)
        recyclerListaConversas.addOnItemTouchListener(RecyclerItemClickListener(
            activity, recyclerListaConversas,
            object : RecyclerItemClickListener.OnItemClickListener {
                 override fun onItemClick(view: View?, position: Int) {
                    val listaConversasAtualizada: List<Conversa> = conversasAdapter!!.getConversas()!!
                    val conversaSelecionada: Conversa = listaConversasAtualizada[position]
                    proximaTelaConversa(conversaSelecionada)
                }

                override fun onLongItemClick(view: View?, position: Int) {
                    try {
                        val listaConversasAtualizada: List<Conversa> =
                            conversasAdapter!!.getConversas()!!
                        val conversaSelecionada: Conversa = listaConversasAtualizada[position]
                        if (conversaSelecionada.isGroup!!.equals("false")) {
                            Alerta.show(
                                activity!!,
                                "Deletar Conversa",
                                "Deseja realmente deletar esta conversa?",
                                "Sim",
                                DialogInterface.OnClickListener { dialog, which ->
                                    conversaSelecionada.idRemetente?.let {
                                        database!!.child("conversas")
                                            .child(it)
                                            .child(conversaSelecionada.idDestinatario!!)
                                            .removeValue()
                                    }
                                    listaConversas.removeAt(position)
                                    conversasAdapter!!.notifyDataSetChanged()
                                },
                                false
                            )
                        } else {
                            Toast.makeText(
                                activity,
                                "Group is not permited to remove",
                                Toast.LENGTH_LONG
                            ).show()
                        }
                    }catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }
        ))
        recyclerListaMsg.addOnItemTouchListener(
            RecyclerItemClickListener(
                activity, recyclerListaMsg,
                object : RecyclerItemClickListener.OnItemClickListener {
                     override fun onItemClick(view: View?, position: Int) {
                        val listaMensagemAtualizada: List<Mensagem?> =
                            conversasMensagemAdapter!!.getConversas()!!
                        val mensagem: Mensagem = listaMensagemAtualizada[position]!!
                        conversasRefBusca =
                            conversasRefBusca!!.child(mensagem.comQuemEstouFalando!!)
                        conversasRefBusca!!.addListenerForSingleValueEvent(object :
                            ValueEventListener {
                            override fun onDataChange(dataSnapshot: DataSnapshot) {
                                if (dataSnapshot.exists()) {
                                    try {
                                        val conversas: Conversa? =
                                            dataSnapshot.getValue(Conversa::class.java)
                                        conversas!!.chaveMensagem = mensagem.id
                                        proximaTelaConversa(conversas)
                                    } catch (e: Exception) {
                                        e.printStackTrace()
                                    }
                                }
                            }

                            override fun onCancelled(databaseError: DatabaseError) {}
                        })
                    }

                    override fun onLongItemClick(view: View?, position: Int) {
                        TODO("Not yet implemented")
                    }
                })
        )
        conversasRef = database!!.child("conversas").child(identificadorUsuario)
        qConversa = conversasRef!!.orderByChild("lido")
        mensagemRef = database!!.child("mensagens")
            .child(mLogin!!.codPessoa.toString())
        database!!.keepSynced(true)
        return view
    }

    override fun onStart() {
        super.onStart()
        recuperarConversas()
    }

    override fun onStop() {
        super.onStop()
//        conversasRef!!.removeEventListener(childEventListenerConversar!!)
    }


    fun pesquisarConversas(texto: String) {
        if (listaMensagens.size > 0 && texto.length > 0) {
            txtMensagem!!.visibility = View.VISIBLE
            recyclerListaMsg!!.visibility = View.VISIBLE
            txtChat!!.visibility = View.VISIBLE
        }
        val listaConversarBusca: ArrayList<Conversa> = ArrayList<Conversa>()
        buscarMensagens(texto)
        for (conversa in listaConversas) {
            if (conversa.usuarioExibicao != null) {
                val nome: String = conversa.usuarioExibicao!!.getNome()!!.toLowerCase()
                //String ultimaMensagem = conversa.getUltimaMensagem().toLowerCase();
                if (nome.contains(texto)) {
                    listaConversarBusca.add(conversa)
                }
            } else {
                val nome: String = conversa.grupo!!.nome!!
                //String ultimaMensagem = conversa.getUltimaMensagem().toLowerCase();
                if (nome.contains(texto)) {
                    listaConversarBusca.add(conversa)
                }
            }
        }
        conversasAdapter = ConversasAdapter(listaConversarBusca!!, getActivity())
        recyclerListaConversas!!.adapter = conversasAdapter
        conversasAdapter!!.notifyDataSetChanged()
    }

    fun recarregarConversar() {
        conversasAdapter = ConversasAdapter(listaConversas, getActivity())
        recyclerListaConversas!!.adapter = conversasAdapter
        conversasAdapter!!.notifyDataSetChanged()
    }


    fun recuperarConversas() {
        listaConversas.clear()
        //childEventListenerConversar =
        val childEventListener = object : ChildEventListener {
            override fun onChildAdded(dataSnapshot: DataSnapshot, previousChildName: String?) {

                val conversasAdd = dataSnapshot.getValue<Conversa>()
                if (conversasAdd != null) {
                    listaConversas.add(conversasAdd)
                }
                conversasAdapter!!.notifyDataSetChanged()
            }

            override fun onChildChanged(dataSnapshot: DataSnapshot, previousChildName: String?) {
                val chave = dataSnapshot.key
                val conversaAlterada: Conversa? = dataSnapshot.getValue<Conversa>()
                for (conversa in listaConversas) {
                    if (chave == conversa.idDestinatario) {
                        listaConversas.remove(conversa)
                        break
                    }
                }
                listaConversas.add(0, conversaAlterada!!)
                conversasAdapter!!.notifyDataSetChanged()

            }

            override fun onChildRemoved(dataSnapshot: DataSnapshot) {
                val conversa: Conversa? = dataSnapshot.getValue<Conversa>()
                listaConversas.remove(conversa)
                conversasAdapter!!.notifyDataSetChanged()
            }

            override fun onChildMoved(dataSnapshot: DataSnapshot, previousChildName: String?) {
                conversasAdapter!!.notifyDataSetChanged()
            }

            override fun onCancelled(databaseError: DatabaseError) {
                conversasAdapter!!.notifyDataSetChanged()
            }
        }
        conversasRef!!.addChildEventListener(childEventListener)
    }

    fun buscarMensagens(texto: String?) {
        val database = FirebaseDatabase.getInstance()
       // val ref = database.getReference("mensagens/" + mLogin!!.codPessoa.toString() + "/")
        val ref = database.getReference("mensagens").child(mLogin!!.codPessoa.toString())
        listaMensagens.clear()
        ref.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                for (`val` in dataSnapshot.children) {
                    try {
                        ref.child(`val`.key!!).addValueEventListener(object : ValueEventListener {
                            override fun onDataChange(dataSnapshot: DataSnapshot) {
                                for (val1 in dataSnapshot.children) {
                                    val msg: Mensagem? = val1.getValue(Mensagem::class.java)
                                    if (msg!!.mensagem != null) {
                                        if (msg.mensagem!!.contains(texto!!)) {
                                            msg.comQuemEstouFalando= `val`.key
                                            listaMensagens.add(msg)
                                        }
                                    }
                                }
                                conversasMensagemAdapter!!.notifyDataSetChanged()
                            }

                            override fun onCancelled(databaseError: DatabaseError) {}
                        })
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }

            override fun onCancelled(databaseError: DatabaseError) {}
            fun onCancelled(firebaseError: FirebaseError?) {}
        })
    }

    fun sumirMensagens() {
        listaMensagens.clear()
        txtMensagem!!.visibility = View.GONE
        recyclerListaMsg!!.visibility = View.GONE
    }

    fun proximaTelaConversa(conversa: Conversa?) {
        val i = Intent(activity, ChatActivity::class.java)
        i.putExtra("chaveMsg", conversa!!.chaveMensagem)
        if (conversa.isGroup.equals("true")) {
            i.putExtra("chatGrupo", conversa.grupo)
            startActivity(i)
        } else {
            i.putExtra("chatContato", conversa.usuarioExibicao)
            startActivity(i)
        }
    }
}