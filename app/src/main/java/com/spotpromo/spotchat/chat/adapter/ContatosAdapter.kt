package com.spotpromo.spotchat.chat.adapter

import android.content.Context
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.spotpromo.spotchat.R
import com.spotpromo.spotchat.model.Contato
import de.hdodenhof.circleimageview.CircleImageView

class ContatosAdapter(val listaContatos: List<Contato>?, val context: Context?) : RecyclerView.Adapter<ContatosAdapter.MyViewHolder>() {



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemLista: View = LayoutInflater.from(context)
            .inflate(R.layout.adapter_contatos, parent, false)
        return MyViewHolder(itemLista)
    }


    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val usuario = listaContatos!![position]
        //validacao Grupo
        var cabecalho = false
        if (usuario.getCodPessoa() == 0) cabecalho = true
        holder.nome.text = usuario.getNome()
        holder.email.text = usuario.getRota()
        if (usuario.getFoto()!!.isNotEmpty()) {
            Glide.with(context!!)
                .load(Uri.parse(usuario.getFoto()))
                .apply(RequestOptions().override(50, 50))
                .into(holder.foto)
        } else {
            if (cabecalho) {
                holder.foto.setImageResource(R.drawable.icone_grupo)
                holder.email.visibility = View.GONE
            } else {
                holder.foto.setImageResource(R.drawable.padrao)
            }
        }
    }

    override fun getItemCount(): Int {
        return listaContatos!!.size
    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var foto: CircleImageView = itemView.findViewById(R.id.imageViewContatoFoto)
        var nome: TextView = itemView.findViewById(R.id.textContatoNome)
        var email: TextView = itemView.findViewById(R.id.textViewContatoEmail)

    }
}