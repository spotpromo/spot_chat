package com.spotpromo.spotchat.chat.activity

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.SearchView
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager.widget.ViewPager
import com.spotpromo.spotchat.chat.fragment.ContatosFragment
import com.spotpromo.spotchat.chat.fragment.ConversasFragment
import com.google.firebase.auth.FirebaseAuth
import com.miguelcatalan.materialsearchview.MaterialSearchView
import com.ogaclejapan.smarttablayout.SmartTabLayout
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItemAdapter
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItems
import com.spotpromo.spotchat.R
import com.spotpromo.spotchat.utils.firebase.ConfiguracaoFirebase
import kotlinx.android.synthetic.main.toolbar.*

class ActivityPrincipalChat : AppCompatActivity()
{
    private var autenticacao: FirebaseAuth? = null
    private var searchView: MaterialSearchView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_chat)

        toolbar.title = "Spot Chat"
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        autenticacao = ConfiguracaoFirebase.getFirebaseAutenticacao()

        val adapter = FragmentPagerItemAdapter(
            supportFragmentManager,
            FragmentPagerItems.with(this)
                .add(resources.getString(R.string.conversas), ConversasFragment::class.java)
                .add(resources.getString(R.string.contatos), ContatosFragment::class.java)
                .create()
        )

        val viewPager = findViewById<ViewPager>(R.id.viewpager)
        viewPager.adapter = adapter
        val viewPageTab: SmartTabLayout = findViewById(R.id.viewPagerTab)
        viewPageTab.setViewPager(viewPager)
        searchView = findViewById(R.id.material_search_principal)

        searchView!!.setOnSearchViewListener(object : MaterialSearchView.SearchViewListener {
            override fun onSearchViewShown() {}
            override fun onSearchViewClosed() {
                val fragment: ConversasFragment = adapter.getPage(0) as ConversasFragment
                fragment.recarregarConversar()
            }
        })

        this.searchView!!.setOnQueryTextListener(object : SearchView.OnQueryTextListener,
            MaterialSearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                when (viewPager.currentItem) {
                    0 -> {
                        val conversasFragment: ConversasFragment =
                            adapter.getPage(0) as ConversasFragment
                        if (newText != null && !newText.isEmpty()) {
                            conversasFragment.pesquisarConversas(newText.toLowerCase())
                        } else {
                            try {
                                conversasFragment.recarregarConversar()
                                conversasFragment.sumirMensagens()
                            } catch (e: Exception) {
                                e.printStackTrace()
                            }
                        }
                    }
                    1 -> {
                        val contatosFragment: ContatosFragment =
                            adapter.getPage(1) as ContatosFragment
                        if (newText != null && !newText.isEmpty()) {
                            contatosFragment.pesquisarContatos(newText.toLowerCase())
                        } else {
                            try {
                                contatosFragment.recarregaContatos()
                            } catch (e: Exception) {
                                e.printStackTrace()
                            }
                        }
                    }
                }
                return true
            }
        })
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_chat, menu)
        val item = menu.findItem(R.id.menuPesquisa)
        this.searchView!!.setMenuItem(item)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        if (id == android.R.id.home) {
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        finish()
    }


}