package com.spotpromo.spotchat.chat.activity

import android.content.Intent
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.spotpromo.spotchat.chat.adapter.ContatosAdapter
import com.spotpromo.spotchat.chat.adapter.GrupoSelecionadoAdapter
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.ValueEventListener
import com.spotpromo.spotchat.R
import com.spotpromo.spotchat.dao.ContatoDAO
import com.spotpromo.spotchat.model.Contato
import com.spotpromo.spotchat.model.Login
import com.spotpromo.spotchat.utils.RecyclerItemClickListener
import com.spotpromo.spotchat.utils.SqliteDataBaseHelper
import com.spotpromo.spotchat.utils.firebase.ConfiguracaoFirebase
import java.io.Serializable
import java.util.*

class GrupoActivity  : AppCompatActivity()
{
    var reclyclerMembrosSelecionados: RecyclerView? =null
    private  var reclyclerMembros:RecyclerView? = null
    var contatosAdapter: ContatosAdapter? = null
    var grupoSelecionadoAdapter: GrupoSelecionadoAdapter? = null
    val listaMembros: MutableList<Contato> = ArrayList<Contato>()
    val listaMembrosSelecionado: MutableList<Contato> = ArrayList<Contato>()
    var usuariosRef: DatabaseReference? = null
    val valueEventListenerMembros: ValueEventListener? = null
    val usuarioAtual: FirebaseUser? = null
    var toolbar: Toolbar? = null
    var fabAvancarCadastro: FloatingActionButton? = null
    var mLogin: Login? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_grupo)
        toolbar = findViewById<Toolbar>(R.id.toolbar)
        toolbar!!.title = "New Group"
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        reclyclerMembros = findViewById<RecyclerView>(R.id.reclyclerMembros)
        reclyclerMembrosSelecionados = findViewById<RecyclerView>(R.id.reclyclerMembrosSelecionados)
        fabAvancarCadastro = findViewById<FloatingActionButton>(R.id.fabSalvarGrupo)
        usuariosRef = ConfiguracaoFirebase.getFirebaseDatabase()!!.child("usuarios")
        //usuarioAtual = UsuarioFirebase.getFirebaseAtual();

        //configuar adapter
        contatosAdapter = ContatosAdapter(listaMembros, applicationContext)
        try {
            mLogin = Login().retornar(this)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        //configurar recylcler view
        val layoutManager = LinearLayoutManager(applicationContext)
        reclyclerMembros!!.layoutManager = layoutManager
        reclyclerMembros!!.setHasFixedSize(true)
        reclyclerMembros!!.adapter = contatosAdapter
        reclyclerMembros!!.addOnItemTouchListener(RecyclerItemClickListener(
            applicationContext, reclyclerMembros!!,
            object : RecyclerItemClickListener.OnItemClickListener {
                override fun onItemClick(view: View?, position: Int) {
                    val usuarioSelecionado: Contato = listaMembros[position]
                    listaMembros.remove(usuarioSelecionado)
                    contatosAdapter!!.notifyDataSetChanged()
                    listaMembrosSelecionado.add(usuarioSelecionado)
                    grupoSelecionadoAdapter!!.notifyDataSetChanged()
                    atualizarMembrosToolbar()
                }

                override fun onLongItemClick(view: View?, position: Int) {}

            }
        ))
        grupoSelecionadoAdapter =
            GrupoSelecionadoAdapter(listaMembrosSelecionado, applicationContext)
        val layoutManagerMembros: RecyclerView.LayoutManager = LinearLayoutManager(
            getApplicationContext(),
            LinearLayoutManager.HORIZONTAL,
            false
        )
        reclyclerMembrosSelecionados!!.layoutManager = layoutManagerMembros
        reclyclerMembrosSelecionados!!.setHasFixedSize(true)
        reclyclerMembrosSelecionados!!.adapter = grupoSelecionadoAdapter
        reclyclerMembrosSelecionados!!.addOnItemTouchListener(RecyclerItemClickListener(
            applicationContext,
            reclyclerMembrosSelecionados!!,
            object : RecyclerItemClickListener.OnItemClickListener {
                 override fun onItemClick(view: View?, position: Int) {
                    val usuarioSelecionado: Contato = listaMembrosSelecionado[position]
                    listaMembrosSelecionado.remove(usuarioSelecionado)
                    grupoSelecionadoAdapter!!.notifyDataSetChanged()
                    listaMembros.add(usuarioSelecionado)
                    contatosAdapter!!.notifyDataSetChanged()
                    atualizarMembrosToolbar()
                }

                override fun onLongItemClick(view: View?, position: Int) {}

            }
        ))
        fabAvancarCadastro!!.setOnClickListener {
            val i = Intent(this@GrupoActivity, CadastroGrupoActivity::class.java)
            i.putExtra("membros", listaMembrosSelecionado as Serializable)
            startActivity(i)
        }
    }

    fun atualizarMembrosToolbar() {
        val totalSelecionados = listaMembrosSelecionado.size
        val total = listaMembros.size + totalSelecionados
        toolbar!!.subtitle = "$totalSelecionados to $total selected"
    }

    override fun onStart() {
        super.onStart()
        try {
            recuperarContatos()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onStop() {
        super.onStop()
//        usuariosRef.removeEventListener( valueEventListenerMembros );
    }


    @Throws(Exception::class)
    fun recuperarContatos() {
        val db: SQLiteDatabase = SqliteDataBaseHelper.openDB(this)
        val daContato = ContatoDAO(db)
        listaMembros.addAll(daContato.select(mLogin!!.codPessoa.toString())!!)
        SqliteDataBaseHelper.closeDB(db)
    }

}