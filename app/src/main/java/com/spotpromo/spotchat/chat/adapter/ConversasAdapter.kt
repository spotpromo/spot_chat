package com.spotpromo.spotchat.chat.adapter

import android.content.Context
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.spotpromo.spotchat.R
import com.spotpromo.spotchat.model.Conversa
import de.hdodenhof.circleimageview.CircleImageView

class ConversasAdapter(val lista: ArrayList<Conversa>?, val context: Context?) : RecyclerView.Adapter<ConversasAdapter.MyViewHolder>()
{


    fun getConversas(): List<Conversa>? {
        return lista
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemLista: View = LayoutInflater.from(parent.context)
            .inflate(R.layout.adapter_conversas, parent, false)
        return MyViewHolder(itemLista)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val conversa: Conversa = lista!![position]
        try {
            holder.ultimaMensagem.text = conversa.ultimaMensagem
            holder.novaMensagem.visibility = View.GONE
            // Log.i("foto",conversa.getUsuarioExibicao().getNome() +"-"+ conversa.getUsuarioExibicao().getFoto().toString());
            if (conversa.lido != null) {
                if (conversa.lido === 0) {
                    holder.novaMensagem.visibility = View.VISIBLE
                }
            }
            if (conversa.isGroup.equals("false")) {
                holder.nome.text = conversa.usuarioExibicao!!.getNome()
                holder.foto.setImageResource(R.drawable.padrao)
                if (conversa.usuarioExibicao!!.getFoto()!!.isNotEmpty()) {
                    Glide.with(context!!)
                        .load(Uri.parse(conversa.usuarioExibicao!!.getFoto()))
                        .apply(RequestOptions().override(50, 50))
                        .into(holder.foto)
                } else {
                    holder.foto.setImageResource(R.drawable.padrao)
                }
            } else {
                holder.nome.text = conversa.grupo!!.nome
                holder.foto.setImageResource(R.drawable.padrao)
                if (conversa.grupo!!.foto != null) {
                    Glide.with(context!!)
                        .load(Uri.parse(conversa.grupo!!.foto))
                        .apply(RequestOptions().override(50, 50))
                        .into(holder.foto)
                } else {
                    holder.foto.setImageResource(R.drawable.padrao)
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun getItemCount(): Int {
        return lista!!.size
    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var foto: CircleImageView = itemView.findViewById(R.id.imageViewContatoFoto)
        var nome: TextView = itemView.findViewById(R.id.textContatoNome)
        var ultimaMensagem: TextView = itemView.findViewById(R.id.textViewContatoEmail)
        var novaMensagem: ImageView = itemView.findViewById(R.id.imageNovaMensagem)

    }
}