package com.spotpromo.spotchat.chat.fragment

import android.content.Context
import android.content.Intent
import android.database.sqlite.SQLiteDatabase
import android.os.AsyncTask
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.spotpromo.spotchat.chat.activity.ChatActivity
import com.spotpromo.spotchat.chat.activity.GrupoActivity
import com.spotpromo.spotchat.chat.adapter.ContatosAdapter
import com.spotpromo.spotchat.utils.SqliteDataBaseHelper
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.ValueEventListener
import com.spotpromo.spotchat.R
import com.spotpromo.spotchat.config.ChatConfig
import com.spotpromo.spotchat.controller.CallBack_Projeto
import com.spotpromo.spotchat.dao.ContatoDAO
import com.spotpromo.spotchat.model.Contato
import com.spotpromo.spotchat.model.Login
import com.spotpromo.spotchat.model.Mensagem
import com.spotpromo.spotchat.utils.LogTrace
import com.spotpromo.spotchat.utils.RecyclerItemClickListener
import com.spotpromo.spotchat.utils.firebase.ConfiguracaoFirebase
import kotlinx.android.synthetic.main.fragment_contatos.*
import java.util.*

class ContatosFragment : DialogFragment(), CallBack_Projeto {
    private var adapter: ContatosAdapter? = null
    private val listaContatos =  ArrayList<Contato>()
    private var usuariosRef: DatabaseReference? = null
    private val valueEventListenerContatos: ValueEventListener? = null
    private val usuarioAtual: FirebaseUser? = null
    private var mLogin: Login? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view: View = inflater.inflate(R.layout.fragment_contatos, container, false)

        //Configurações iniciais
        usuariosRef = ConfiguracaoFirebase.getFirebaseDatabase()!!.child("usuarios")

        //configurar adapter
        adapter = ContatosAdapter(listaContatos, context!!)
        try {
            mLogin = Login().retornar(activity!!)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        //configurar recyclerview
        val layoutManager = LinearLayoutManager(this.context!!)
        val recylcerViewListaContatos = view.findViewById<RecyclerView>(R.id.recylcerViewListaContatos)
        recylcerViewListaContatos.layoutManager = layoutManager
        recylcerViewListaContatos.setHasFixedSize(true)
        recylcerViewListaContatos.adapter = adapter

        recylcerViewListaContatos.addOnItemTouchListener(RecyclerItemClickListener(
            activity, recylcerViewListaContatos,
            object : RecyclerItemClickListener.OnItemClickListener {
                 override fun onItemClick(view: View?, position: Int) {
                    val listaUsuarioAtualizada = adapter!!.listaContatos
                    val usuarioSelecionado: Contato = listaUsuarioAtualizada!![position]
                    var cabecalho = false
                    if (usuarioSelecionado.getCodPessoa() == 0) cabecalho = true
                    if (cabecalho) {
                        val i = Intent(activity, GrupoActivity::class.java)
                        startActivity(i)
                    } else {
                        val i = Intent(activity, ChatActivity::class.java)
                        if (usuarioSelecionado.getFoto() == null) {
                            usuarioSelecionado.setFoto(ChatConfig.url_default_chat)
                        }
                        i.putExtra("chatContato", usuarioSelecionado)
                        startActivity(i)
                    }
                }

                override fun onLongItemClick(view: View?, position: Int) {}
            }
        ))

        // adicionarMenuNovoGrupo();
        return view
    }

    override fun onRetornoFotoGrupo(aBolean: Boolean, data: String?, mensagem: String?) {
        TODO("Not yet implemented")
    }

    override fun onRetorno(mensagem: Mensagem) {
        TODO("Not yet implemented")
    }

    override fun onRetorno(aBolean: Boolean, data: String?, mensagem: String?) {
        TODO("Not yet implemented")
    }

    override fun onRetorno(
        aBolean: Boolean,
        data: String?,
        mensagem: String?,
        nomeArquivo: String?
    ) {
        TODO("Not yet implemented")
    }

    override fun onRetorno(aBoolean: Boolean, mensagem: String) {
        TODO("Not yet implemented")
    }

    override fun onStart() {
        super.onStart()
        try {
            recuperarContatos()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onStop() {
        super.onStop()
//        usuariosRef.removeEventListener( valueEventListenerContatos );
    }

    fun limparContatos() {
        listaContatos.clear()
        adicionarMenuNovoGrupo()
    }

    fun adicionarMenuNovoGrupo() {
        val itemGrupo = Contato()
        itemGrupo.setCodPessoa(0)
        itemGrupo.setNome("New Group")
        itemGrupo.setRota("")
        listaContatos.add(itemGrupo)
    }


    class Task(private val context: Context, private val listaContatos : ArrayList<Contato?>) :
        AsyncTask<Int?, Void?, Boolean>() {
        private val callback: CallBack_Projeto = context as CallBack_Projeto
        private var mensagem: String? = null
        override fun onPreExecute() {
            super.onPreExecute()
            listaContatos?.clear()
        }

        override fun doInBackground(vararg integers: Int?): Boolean? {
            try {
                val mLogin = Login().retornar(context!!)
                val db: SQLiteDatabase = SqliteDataBaseHelper.openDB(context)
                val contatoDAO = ContatoDAO(db)
                listaContatos.addAll(contatoDAO.select(mLogin!!.codPessoa.toString())!!)
                SqliteDataBaseHelper.closeDB(db)
                if (listaContatos.isNotEmpty()) return true
            } catch (err: Exception) {
                mensagem = LogTrace.logCatch(context, this.javaClass, err, false)
            }
            return false
        }

        override fun onPostExecute(aBoolean: Boolean) {
            super.onPostExecute(aBoolean)
            //hideProgress();
            callback.onRetorno(aBoolean, mensagem!!)
        }

    }


    fun pesquisarContatos(texto: String?) {
        val listaContatosBusca: MutableList<Contato> = ArrayList<Contato>()
        for (contato in listaContatos) {
            if (contato.getNome() != null) {
                val nome: String = contato.getNome()!!.toLowerCase()
                val ultimaMensagem: String = contato.getRota()!!.toLowerCase()
                if (nome.contains(texto!!) || ultimaMensagem.contains(texto)) {
                    listaContatosBusca.add(contato)
                }
            }
        }
        adapter = ContatosAdapter(listaContatosBusca, getActivity())
        recylcerViewListaContatos!!.adapter = adapter
        adapter!!.notifyDataSetChanged()
    }

    fun recarregaContatos() {
        adapter = ContatosAdapter(listaContatos, activity)
        recylcerViewListaContatos!!.adapter = adapter
        adapter!!.notifyDataSetChanged()
    }

    @Throws(Exception::class)
    fun recuperarContatos() {
        val db: SQLiteDatabase = SqliteDataBaseHelper.openDB(context!!)
        val daContato = ContatoDAO(db)
        listaContatos.addAll(daContato.select(mLogin!!.codPessoa.toString())!!)
        SqliteDataBaseHelper.closeDB(db)
    }
}