package com.spotpromo.spotchat.controller

import com.spotpromo.spotchat.model.Mensagem


interface CallBack_Projeto {
    fun onRetornoFotoGrupo(aBolean: Boolean, data: String?, mensagem: String?)
    fun onRetorno(mensagem: Mensagem)
    fun onRetorno(aBolean: Boolean, data: String?, mensagem: String?)
    fun onRetorno(aBolean: Boolean, data: String?, mensagem: String?, nomeArquivo: String?)
    fun onRetorno(aBoolean: Boolean, mensagem: String)
}